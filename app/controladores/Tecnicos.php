<?php
    class Tecnicos extends Controlador
    {
        public function __construct(){
            Sesion::start();
            $this->tecnicoModelo = $this->modelo('Tecnico');
            $this->cuadrillaModelo = $this->modelo('Cuadrilla');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $tecnicos = $this->tecnicoModelo->getTecnicos();
                $datos = [
                    'tecs'=>$tecnicos
                ];
                $this->vista('/tecnicos/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //crear tecnico
        public function crear(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $cuadrillas = $this->cuadrillaModelo->obtenercuadrillas();

                $datos = [
                    'cuadrillas'=>$cuadrillas
                ];
                $this->vista('/tecnicos/crear',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //guarda un tecnico
        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $datos = [
                        'nombre' => $_POST['nombres'],
                        'direccion' => $_POST['direccion'],
                        'correo' => $_POST['correo'],
                        'telefono' => $_POST['telefono'],
                        'dui' => $_POST['dui'],
                        'cuad' => $_POST['cuad']
                    ];

                    if($this->tecnicoModelo->agregarTecnico($datos)){
                        redireccionar('/tecnicos');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                    
                }
                $this->vista('/tecnicos/crear');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //edita un tecnico
        public function editar($id){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if (is_numeric($id)) {
                    $tecnico = $this->tecnicoModelo->obtenerTecnicoId($id);
                    $cuadrillas = $this->cuadrillaModelo->obtenercuadrillas();
                    $datos = [
                        'id'=>$id,
                        'nombre'=>$tecnico->nombre,
                        'dir'=>$tecnico->direccion,
                        'email'=>$tecnico->correo,
                        'tel'=>$tecnico->tel,
                        'dui'=>$tecnico->dui,
                        'cuadrilla'=>$tecnico->cuadrilla,
                        'codecuadrilla'=>$tecnico->codecuad,
                        'cuadrillas'=>$cuadrillas
                    ];
                    $this->vista('/tecnicos/editar',$datos);
                }
                redireccionar('/tecnicos/index');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //actualiza un tecnico
        public function update($id){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (is_numeric($id)) {
                        $datos = [
                            'id'=>$id,
                            'nombre' => $_POST['nombres'],
                            'direccion' => $_POST['direccion'],
                            'correo' => $_POST['correo'],
                            'telefono' => $_POST['telefono'],
                            'dui' => $_POST['dui'],
                            'cuad' => $_POST['cuad']
                        ];

                        if($this->tecnicoModelo->updateTecnico($datos)){
                            redireccionar('/tecnicos');
                        }else{
                            die('Ocurrio un Problema al Insertar los datos');
                        }

                    }
                    redireccionar('/tecnicos/');
                    
                }
                $this->vista('/tecnicos/index');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //elimina un tecnico
        public function eliminar(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $datos = [
                        'id'=>$_POST['id']
                    ];

                    if ($this->tecnicoModelo->eliminarTecnico($datos)) {
                        echo '1';//se elimino correctamente
                    }else{
                        echo '0';//error al eliminar registro
                    }
                }else{
                    echo '3';//post no enviado
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        
    }
    