$(document).ready(function(){
    $('#e').attr('disabled',true);

    $(document).on('click','.delete',function(){
        let element = $(this)[0].parentElement.parentElement;
        let idtec = $(element).attr('inde');
       $('#e').val(idtec);
        $('#elimproducto').modal('show');
    });

    $(document).on('click','#borrarprod',function(){
        let url = $('#ruta1').val();
        let id = $('#e').val();

        let posDatos = {
            id:id
        };

        $.post(url+'/productos/eliminar',posDatos,function(response){
            if (response == 1) {
                toastr.success('Registro eliminado correctamente.');
                $('#e').val('');
                $('#elimproducto').modal('hide');
                $('#okis').modal('show');
            }else{
                toastr.error('Error al eliminar registro, recargue la pagina e intente de nuevo.');
            }
        });
    });
});