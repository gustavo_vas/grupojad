<?php
    class Proyecto
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //obtener productos
        public function getproyectos(){
            $consulta = "SELECT (pro.id_proyecto)AS CODE,(pro.id_tipo_proyecto)AS codetipopro,(pro.id_cuadrilla)AS codecuadrilla,
                (pro.numero_proyecto)AS numpro,ifnull(DATE_FORMAT(pro.fecha_creacion,'%d/%m/%Y'),'N/A')AS fcreado,
                ifnull(DATE_FORMAT(pro.fecha_actualizado,'%d/%m/%Y'),'N/A')AS factualizado,ifnull(pro.fecha_eliminado,'N/A')AS feliminado,
                (tp.descripcion)AS tipopro,(cd.nombre_cuadrilla)AS cuadrilla,(cd.encargado_cuadrilla)AS encargado 
                FROM proyectos pro 
                JOIN tipo_proyecto tp ON pro.id_tipo_proyecto=tp.id_tipo_proyecto 
                JOIN cuadrillas cd ON pro.id_cuadrilla=cd.id_cuadrilla 
                ORDER BY pro.fecha_creacion asc";
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //guardar un proyecto
        public function agregarProyecto($datos){
            $consulta = 'INSERT INTO proyectos (numero_proyecto,fecha_creacion,id_tipo_proyecto,id_cuadrilla) 
                    values(:numero,:fcreado,:tiposp,:cuadrilla)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':numero',$datos['numero']);
            $this->db->bind(':fcreado',$datos['fcreado']);
            $this->db->bind(':tiposp',$datos['tiposp']);
            $this->db->bind(':cuadrilla',$datos['cuadrilla']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //obtener proyecto por id
        public function obtenerProyectoId($id){
            $consulta = "SELECT (pro.id_proyecto)AS code,(pro.id_tipo_proyecto)AS codetipopro,(pro.id_cuadrilla)AS codecuadrilla,
                (pro.numero_proyecto)AS numpro,ifnull(pro.fecha_creacion,'N/A')AS fcreado,
                ifnull(pro.fecha_actualizado,'N/A')AS factualizado,ifnull(pro.fecha_eliminado,'N/A')AS feliminado,
                (tp.descripcion)AS tipopro,(cd.nombre_cuadrilla)AS cuadrilla,(cd.encargado_cuadrilla)AS encargado 
                FROM proyectos pro 
                JOIN tipo_proyecto tp ON pro.id_tipo_proyecto=tp.id_tipo_proyecto 
                JOIN cuadrillas cd ON pro.id_cuadrilla=cd.id_cuadrilla 
                where pro.id_proyecto=:id
                ORDER BY pro.fecha_creacion asc";
            
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        //modificar un proyecto
        public function updateProyecto($datos){
            $consulta = 'UPDATE proyectos set numero_proyecto=:numero,fecha_actualizado=:fupdate,
                    id_tipo_proyecto=:tiposp,id_cuadrilla=:cuadrilla 
                    where id_proyecto = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':numero',$datos['numero']);
            $this->db->bind(':fupdate',$datos['fupdate']);
            $this->db->bind(':tiposp',$datos['tiposp']);
            $this->db->bind(':cuadrilla',$datos['cuadrilla']);
            $this->db->bind(':id',$datos['id']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //elimina un proyecto
        public function eliminar($datos){
            $consulta = 'DELETE FROM proyectos where id_proyecto=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['id']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    }
    