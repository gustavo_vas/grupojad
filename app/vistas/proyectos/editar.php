<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Modificar Proyectos</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5">
                                <form action="<?php echo RUTA_URL; ?>/proyectos/update/<?php echo $datos['id']; ?>" method="post">
                                    <div class="form-group row">
                                        <label for="numero" class="col-sm-4">Número</label>
                                        <input type="text" class="form-control col-sm-8" name="numero" placeholder="Ingresar Número de proyecto" required="true" 
                                            value="<?php echo $datos['numpro']; ?>">    
                                    </div>

                                    <div class="form-group row">
                                        <label for="fcreado" class="col-sm-4">Fecha Actualizado</label>
                                        <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control col-sm-8" name="fupdate" placeholder="Ingresar Fecha de creacion" required="true">  
                                    </div>

                                    <div class="form-group row">
                                        <label for="cuadrilla" class="col-sm-4">Cuadrilla</label>
                                        <select class="form-control col-sm-8" name="cuadrilla" required="true">
                                            <?php 
                                                echo '<option value="'.$datos['codecuadrilla'].'">'.$datos['cuadrilla'].'</option>';
                                                foreach ($datos['cuadrillas'] as $cuadrilla) {
                                                    echo '<option value="'.$cuadrilla->id_cuadrilla.'">'.$cuadrilla->nombre_cuadrilla.'</option>';
                                                }
                                            ?>
                                        </select>    
                                    </div>

                                    <div class="form-group row">
                                        <label for="tiposp" class="col-sm-4">Tipo Proyecto</label>
                                        <select class="form-control col-sm-8" name="tiposp" required="true">
                                            <?php 
                                                echo '<option value="'.$datos['codetipop'].'">'.$datos['tiposps'].'</option>';
                                                foreach ($datos['tiposp'] as $tipo) {
                                                    echo '<option value="'.$tipo->id_tipo_proyecto.'">'.$tipo->descripcion.'</option>';
                                                }
                                            ?>
                                        </select>    
                                    </div>
                                
                                <button type="submit" class="btn btn-primary">Registrar</button>
                                <button type="reset" class="btn btn-info">Resetear Formulario</button>
                                <a href="<?php echo RUTA_URL; ?>/proyectos/">
                                    <button type="button" class="btn btn-danger">Cancelar</button>
                                </a>
                                </form>
                            </div>
                        </div>
                    </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
    </body>
</html>