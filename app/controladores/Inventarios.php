<?php 
    class Inventarios extends Controlador
    {
        public function __construct(){
            Sesion::start();
            $this->inventarioModelo = $this->modelo('Inventario');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventarios();
                $tipoInvent = 'General';
                $tipoInvents = 'Seriado';
                $tipoInventns = 'No Seriado';
                $urls = '/inventarios/seriado';
                $urlns = '/inventarios/noseriado';
                $urlng = '/inventarios/reporte';
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent,
                    'urls'=>$urls,
                    'urlns'=>$urlns,
                    'urlng'=>$urlng,
                    'tipos'=>$tipoInvents,
                    'tipons'=>$tipoInventns
                ];
                $this->vista('/inventarios/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function seriado(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventariosSeriados();
                $tipoInvent = 'Seriado';
                $tipoInvents = 'General';
                $tipoInventns = 'No Seriado';
                $urls = '/inventarios/index';
                $urlns = '/inventarios/noseriado';
                $urlng = '/inventarios/reportes';
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent,
                    'urls'=>$urls,
                    'urlns'=>$urlns,
                    'urlng'=>$urlng,
                    'tipos'=>$tipoInvents,
                    'tipons'=>$tipoInventns
                ];
                $this->vista('/inventarios/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function noseriado(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventariosNoSeriados();
                $tipoInvent = 'No Seriado';
                $tipoInventns = 'Seriado';
                $tipoInvents = 'General';
                $urlns = '/inventarios/seriado';
                $urlng = '/inventarios/reportens';
                $urls = '/inventarios/index';
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent,
                    'urls'=>$urls,
                    'urlns'=>$urlns,
                    'urlng'=>$urlng,
                    'tipos'=>$tipoInvents,
                    'tipons'=>$tipoInventns
                ];
                $this->vista('/inventarios/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function reporte(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventarios();  
                $tipoInvent = 'GENERAL';              
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent
                ];
                $this->vista('/inventarios/informeno',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function reportes(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventariosSeriados();   
                $tipoInvent = 'SERIADO';             
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent
                ];
                $this->vista('/inventarios/informeno',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function reportens(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventariosNoSeriados();  
                $tipoInvent = 'NO SERIADO';               
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent
                ];
                $this->vista('/inventarios/informeno',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function bodega(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventarios();
                $tipoInvent = 'General';
                $tipoInvents = 'Seriado';
                $tipoInventns = 'No Seriado';
                $urls = '/inventarios/seriado';
                $urlns = '/inventarios/noseriado';
                $urlng = '/inventarios/reportebo';
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent,
                    'urls'=>$urls,
                    'urlns'=>$urlns,
                    'urlng'=>$urlng,
                    'tipos'=>$tipoInvents,
                    'tipons'=>$tipoInventns
                ];
                $this->vista('/inventarios/bodega',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function reportebo(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventarios();  
                $tipoInvent = 'GENERAL';              
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent
                ];
                $this->vista('/inventarios/informenobo',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function ruta(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventarios();
                $tipoInvent = 'General';
                $tipoInvents = 'Seriado';
                $tipoInventns = 'No Seriado';
                $urls = '/inventarios/seriado';
                $urlns = '/inventarios/noseriado';
                $urlng = '/inventarios/reporteru';
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent,
                    'urls'=>$urls,
                    'urlns'=>$urlns,
                    'urlng'=>$urlng,
                    'tipos'=>$tipoInvents,
                    'tipons'=>$tipoInventns
                ];
                $this->vista('/inventarios/ruta',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function reporteru(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventarios();  
                $tipoInvent = 'GENERAL';              
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent
                ];
                $this->vista('/inventarios/informenoru',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function instaladoinv(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventarios();
                $tipoInvent = 'General';
                $tipoInvents = 'Seriado';
                $tipoInventns = 'No Seriado';
                $urls = '/inventarios/seriado';
                $urlns = '/inventarios/noseriado';
                $urlng = '/inventarios/reporteinst';
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent,
                    'urls'=>$urls,
                    'urlns'=>$urlns,
                    'urlng'=>$urlng,
                    'tipos'=>$tipoInvents,
                    'tipons'=>$tipoInventns
                ];
                $this->vista('/inventarios/instaladoinv',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function reporteinst(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $inventarios = $this->inventarioModelo->getinventarios();  
                $tipoInvent = 'GENERAL';              
                $datos = [
                    'inventarios'=>$inventarios,
                    'tipo'=>$tipoInvent
                ];
                $this->vista('/inventarios/informenoinst',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    