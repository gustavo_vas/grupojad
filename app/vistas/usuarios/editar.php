<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Modificar usuario</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        






                        

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5">
                                <form action="<?php echo RUTA_URL; ?>/usuarios/update/<?php echo $datos['id']; ?>" method="post">
                                    <div class="form-group row">
                                        <label for="nombres" class="col-sm-3">Nombre</label>
                                        <input type="text" class="form-control col-sm-9" name="nombres" placeholder="Ingresar nombre" 
                                            value="<?php echo $datos['nombre']; ?>" required="true">    
                                    </div>

                                    <div class="form-group row">
                                        <label for="apellidos" class="col-sm-3">Apellido</label>
                                        <input type="text" class="form-control col-sm-9" name="apellidos" placeholder="Ingresar apellido" 
                                            value="<?php echo $datos['ape']; ?>" required="true">  
                                    </div>

                                <div class="form-group row">
                                    <label for="correo" class="col-sm-3">Correo</label>
                                    <input type="email" class="form-control col-sm-9" name="correo" placeholder="Ingresar Correo" 
                                        value="<?php echo $datos['email']; ?>" required="true"> 
                                </div>

                                <div class="form-group row">
                                    <label for="telefono" class="col-sm-3">Telefono</label>
                                    <input type="text" class="form-control col-sm-9" name="telefono" placeholder="Ingresar telefono" 
                                        value="<?php echo $datos['tel']; ?>" required="true">	    
                                </div>

                                <div class="form-group row">
                                    <label for="tipo_user" class="col-sm-3">Tipo usuario</label>
                                    <select class="form-control col-sm-9" name="tipo_user">
                                        <?php
                                            if ($datos['tipo']==1) {
                                                echo '<option value="1">Administrador</option>';
                                            }else{
                                                echo '<option value="2">Normal</option>';
                                            }
                                        ?>
                                        <option value="1">Administrador</option>
                                        <option value="2">Normal</option>
                                    </select>    
                                </div>

                                <div class="form-group row">
                                    <label for="estado" class="col-sm-3">Estado</label>
                                    <select class="form-control col-sm-9" name="estado">
                                        <?php
                                            if ($datos['estado']==1) {
                                                echo '<option value="1">Activo</option>';
                                            }else{
                                                echo '<option value="2">Inactivo</option>';
                                            }
                                        ?>
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>
                                    </select>    
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Registrar</button>
                                <button type="reset" class="btn btn-info">Resetear formulario</button>
                                <a href="<?php echo RUTA_URL; ?>/usuarios/">
                                    <button type="button" class="btn btn-danger">Cancelar</button>
                                </a>
                                </form>
                            </div>
                        </div>
                    </div>









                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
    </body>
</html>