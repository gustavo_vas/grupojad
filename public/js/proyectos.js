$(document).ready(function(){
    $('#e').attr('disabled',true);

    $(document).on('click','.delete',function(){
        let element = $(this)[0].parentElement.parentElement;
        let idtec = $(element).attr('inde');
       $('#e').val(idtec);
        $('#eliminar').modal('show');
    });

    $(document).on('click','#borrar',function(){
        let url = $('#ruta').val();
        let id = $('#e').val();

        let posDatos = {
            id:id
        };

        $.post(url+'/proyectos/eliminar',posDatos,function(response){
            if (response == 1) {
                toastr.success('Registro eliminado correctamente.');
                $('#e').val('');
                $('#eliminar').modal('hide');
                $('#okis').modal('show');
            }else{
                toastr.error('Error al eliminar registro, verifique que el registro no tenga dependencia en otra tabla.');
            }
        });
    });
});