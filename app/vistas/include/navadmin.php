    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="<?php echo RUTA_URL;?>/administradores/" class="nav-link">Inicio</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Ayuda</a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Buscar" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a href="<?php echo RUTA_URL; ?>/administradores/destroySesion" class="nav-link">
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    Salir
                </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="<?php echo RUTA_URL;?>/img/logo1.jpg" alt="Logo" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                <span class="brand-text font-weight-light">Administrador</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="info">
                        <a href="#" class="d-block"><?php echo Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser'); ?></a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                            with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview menu-open">
                            <a href="<?php echo RUTA_URL;?>/administradores/" class="nav-link active">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Inicio
                                </p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    Administración
                                    <i class="fas fa-angle-left right"></i>
                                    <span class="badge badge-info right">1</span>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/usuarios/" class="nav-link">
                                        <i class="nav-icon fas fa-users"></i>
                                        <p>Control usuarios</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Empleados
                                    <i class="fas fa-angle-left right"></i>
                                    <span class="badge badge-info right">2</span>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/cuadrillas/" class="nav-link">
                                        <i class="nav-icon fas fa-file-alt"></i>
                                        <p>Cuadrillas</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/tecnicos/" class="nav-link">
                                        <i class="nav-icon fas fa-users"></i>
                                        <p>Técnicos</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                                <i class="nav-icon fas fa-parking"></i>
                                <p>
                                    Productos
                                    <i class="fas fa-angle-left right"></i>
                                    <span class="badge badge-info right">2</span>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/marcas/" class="nav-link">
                                        <i class="nav-icon fas fa-landmark"></i>
                                        <p>Marcas</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/productos/" class="nav-link">
                                        <i class="nav-icon fas fa-parking"></i>
                                        <p>Productos</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                                <i class="nav-icon fas fa-boxes"></i>
                                <p>
                                    Inventarios
                                    <i class="fas fa-angle-left right"></i>
                                    <span class="badge badge-info right">3</span>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/inventarios/" class="nav-link">
                                        <i class="nav-icon fas fa-bezier-curve"></i>
                                        <p>Inventario General</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/inventarios/seriado" class="nav-link">
                                        <i class="nav-icon fas fa-bezier-curve"></i>
                                        <p>Inventario Seriado</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/inventarios/noseriado" class="nav-link">
                                        <i class="nav-icon fas fa-bezier-curve"></i>
                                        <p>Inventario No Seriado</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                                <i class="nav-icon fas fa-tools"></i>
                                <p>
                                    Configuracion
                                    <i class="fas fa-angle-left right"></i>
                                    <span class="badge badge-info right">3</span>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/Tproyectos/" class="nav-link">
                                        <i class="nav-icon fas fa-project-diagram"></i>
                                        <p>Tipo Proyecto</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/unidades/" class="nav-link">
                                        <i class="nav-icon fas fa-calculator"></i>
                                        <p>Unidades de productos</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/estados/" class="nav-link">
                                        <i class="nav-icon fas fa-cubes"></i>
                                        <p>Estados de productos</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                                <i class="nav-icon fas fa-archive"></i>
                                <p>
                                    Movimientos
                                    <i class="fas fa-angle-left right"></i>
                                    <span class="badge badge-info right">4</span>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/entradas/" class="nav-link">
                                        <i class="nav-icon fas fa-download"></i>
                                        <p>Entrada de material</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/salidas/" class="nav-link">
                                        <i class="nav-icon fas fa-upload"></i>
                                        <p>Salida y entrega de material</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/proyectos/" class="nav-link">
                                        <i class="nav-icon fas fa-project-diagram"></i>
                                        <p>Proyectos</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/devoluciones/" class="nav-link">
                                        <i class="nav-icon fas fa-truck"></i>
                                        <p>Devoluciones</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    Instalaciones
                                    <i class="fas fa-angle-left right"></i>
                                    <span class="badge badge-info right">1</span>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/salidas/instalaciones" class="nav-link">
                                        <i class="nav-icon fas fa-cogs"></i>
                                        <p>Instalación de material</p> 
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                                <i class="nav-icon fas fa-print"></i>
                                <p>
                                    Informes
                                    <i class="fas fa-angle-left right"></i>
                                    <span class="badge badge-info right">3</span>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/inventarios/bodega" class="nav-link">
                                        <i class="nav-icon fas fa-print"></i>
                                        <p>Inventario Bodega</p> 
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/inventarios/ruta" class="nav-link">
                                        <i class="nav-icon fas fa-print"></i>
                                        <p>Inventario Ruta</p> 
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo RUTA_URL; ?>/inventarios/instaladoinv" class="nav-link">
                                        <i class="nav-icon fas fa-print"></i>
                                        <p>Inventario Instalado</p> 
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>