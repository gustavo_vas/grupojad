<?php
    class Salida
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        public function getSalidas(){
            $consulta = "SELECT (s.id_salida)as code, (s.documento)as documento, ifnull(DATE_FORMAT(s.fecha_entrega,'%d/%m/%Y'),'N/A')as fentrega, 
                        ifnull(DATE_FORMAT(s.fecha_actualizado,'%d/%m/%Y'),'N/A')as fupdate, 
                        (s.id_proyecto)as codeproyecto, (pr.numero_proyecto)as numproyecto,
                        (pr.id_cuadrilla)as codecuadrilla,(cd.nombre_cuadrilla)as nomcuads
                        FROM salidas s
                        join proyectos pr on s.id_proyecto = pr.id_proyecto
                        join cuadrillas cd on pr.id_cuadrilla = cd.id_cuadrilla
                        order by s.fecha_entrega asc";

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //metodo para agregar un maestro de entradas
        public function agregarSalida($datos){
            $dev = 'CALL addsalida(:doc, :fcreado, :fentrega, :usuario, :proy, :pentrega, :precibe)';
            $this->db->query($dev);

            $this->db->bind(':doc',$datos['documento']);
            $this->db->bind(':fcreado',$datos['fcreado']);
            $this->db->bind(':fentrega',$datos['fentrega']);
            $this->db->bind(':usuario',$datos['usuario']);
            $this->db->bind(':proy',$datos['proyectos']);
            $this->db->bind(':pentrega',$datos['pentrega']);
            $this->db->bind(':precibe',$datos['precibe']);

            $resultado = $this->db->registro();
            $idsalida = $resultado->id;
            return $idsalida;
        }

        public function salidainventario($datos){
            $consulta = 'CALL salidainventario(:idinv,:cantidad,:idsalida);';
            $this->db->query($consulta);

                //vincular los valores
            $this->db->bind(':idinv',$datos['idinv']);
            // $this->db->bind(':estado',$datos['estado']);
            $this->db->bind(':cantidad',$datos['cantidad']);
            $this->db->bind(':idsalida',$datos['idsalida']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function getSalida($id){
            $consulta = "SELECT (s.id_salida)as code, (s.documento)as documento, 
                        ifnull(DATE_FORMAT(s.fecha_entrega,'%d/%m/%Y'),'N/A')as fentrega, 
                        ifnull(DATE_FORMAT(s.fecha_actualizado,'%d/%m/%Y'),'N/A')as fupdate,(py.numero_proyecto)as nproy,
                        (s.persona_entrega)as pentrega, (s.persona_recibe)as precibe,
                        (py.id_cuadrilla)as codecuadrilla,(cd.nombre_cuadrilla)as nomcuads
                        FROM salidas s
                        join proyectos py on s.id_proyecto=py.id_proyecto
                        join cuadrillas cd on py.id_cuadrilla = cd.id_cuadrilla
                        where s.id_salida=:id
                        order by s.fecha_entrega asc";

            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        public function getDetalle($id){
            $consulta = "SELECT (dt.id_detalle_salida)AS idet,(p.codigo)AS codigo, (p.nombre)AS producto,
                        (p.descripcion)AS descripciono,
                        (p.tipo_producto)AS tipo, (dt.cantidad)AS cantidad, (dt.cant_instalado)AS instalado,
                        (dt.cant_devuelto)AS devuelto  
                        FROM detalle_salidas dt
                        JOIN inventario inv ON dt.id_inventario=inv.id_inventario
                        JOIN productos p ON inv.id_producto=p.id_producto                        
                        WHERE id_salida=:id";

            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function addinstall($datos){
            $consulta = 'CALL addinstall(:iddeta,:cantidad);';
            $this->db->query($consulta);

                //vincular los valores
            $this->db->bind(':iddeta',$datos['detid']);
            // $this->db->bind(':estado',$datos['estado']);
            $this->db->bind(':cantidad',$datos['instalcant']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
        
    }
    