<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalle de entrega</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">

                        <div class="jumbotron">
                            <div class="row">
                                <h4 class="display-6 col-md-4">No. Documento: <?php echo $datos['documento']; ?></h4>
                                <p class="lead col-md-4">Fecha de entrega: <?php echo $datos['fentrega']; ?></p>
                                <p class="lead col-md-4">No. de Proyecto: <?php echo $datos['nproy']; ?></p>
                            </div>
                            <div class="row">
                                <p class="lead col-md-4">Entregado por: <?php echo $datos['pentrega']; ?></p>
                                <p class="lead col-md-4">Recibido por: <?php echo $datos['precibe']; ?></p>
                            </div>
                            <hr class="my-4">
                            <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-sm table-hover" id="t">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Producto</th>
                                                <th>Descripción</th>
                                                <th>Tipo</th>
                                                <th>Cant. Ruta</th>
                                                <th>Cant. Instalado</th>
                                                <th>Cant. Devuelto</th>
                                                <th>Cant. Sobrante</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="table-detalle">
                                            <?php $total=0; foreach($datos['detalle'] as $detalle){
                                                $total += $detalle->cantidad;
                                                if ($detalle->tipo == 1) {
                                                    $tipo = 'Seriado';
                                                }else{
                                                    $tipo = 'No Seriado';
                                                }
                                                $sobrante=($detalle->cantidad - ($detalle->instalado+$detalle->devuelto));

                                                echo '<tr inder="'.$detalle->idet.'">';
                                                    echo '<td>'.$detalle->codigo.'</td>';
                                                    echo '<td>'.$detalle->producto.'</td>';
                                                    echo '<td>'.$detalle->descripciono.'</td>';
                                                    echo '<td>'.$tipo.'</td>';
                                                    echo '<td><b>'.$detalle->cantidad.'</b></td>';
                                                    echo '<td><b>'.$detalle->instalado.'</b></td>';
                                                    echo '<td><b>'.$detalle->devuelto.'</b></td>';
                                                    echo '<td><b>'.$sobrante.'</b></td>';
                                                    echo '<td><button type="button" id="btninstall" class="btn btn-primary">Instalar</button></td>';
                                                echo '</tr>';
                                            }?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td style="text-align: right;" colspan="4"><b>TOTAL</b></td>
                                                <td id="total"><?php echo $total; ?></td>
                                            </tr>
                                        <tfoot>
                                    </table>
                                </div>
                            <a class="btn btn-primary btn-lg" href="<?php echo RUTA_URL; ?>/salidas/instalaciones" role="button">Regresar</a>
                        </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->

        <!-- modal para editar marca  -->
        <div class="modal fade" id="install">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Cantidad a instalar</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">

                            <div class="form-group row">
                            <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                <div class="col-sm-3">
                                    <label for="code" class="control-label" id="lblcode">ID</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="iddet" required="true" placeholder="Inventario">
                                    <label id="code1" style="color:red;"></label>
                                </div>
                            </div>
                                                    
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="code" class="control-label" id="lblcode">Codigo</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="codeg" required="true" placeholder="Codigo de producto">
                                    <label id="code1" style="color:red;"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="descripcion1" class="control-label" id="lbldescripcion">Cant. Ruta</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="cantruta" required="true" placeholder="Cantidad en ruta">
                                    <label id="desc1" style="color:red;"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="descripcion1" class="control-label" id="lbldescripcion">Sobrante</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="sobra" required="true" placeholder="Cantidad en ruta">
                                    <label id="desc1" style="color:red;"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="cant1" class="control-label" id="lblcant1">Cant. Instalar</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="cantinstall" required="true" placeholder="Cantidad a instalar">
                                    <label id="canti1" style="color:red;"></label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button id="btninstallprod" class="btn btn-primary">Instalar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/salidas.js"></script>
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#t').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                });
            });
        </script>
    </body>
</html>