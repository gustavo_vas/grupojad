<?php
    class Tproyecto
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        public function getTipos(){
            $consulta = 'SELECT * FROM tipo_proyecto order by descripcion asc';
            $this->db->query($consulta);

            $resultado = $this->db->registros();
            return $resultado;
        }

        //guardar tipo proyecto
        public function agregartp($datos){
            $consulta = 'INSERT INTO tipo_proyecto (descripcion) 
                    values(:descripcion)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':descripcion',$datos['descripcion']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //obtener cantidad tipo proyectos por descripcion
        public function obtenercanttpdesc($desc){
            $consulta = 'SELECT * FROM tipo_proyecto WHERE descripcion=:descripcion';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$desc);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad tipo proyecto por descripcion para editar
        public function obtenercanttpdescnotid($descripcion,$id){
            $consulta = 'SELECT * FROM tipo_proyecto 
                        WHERE descripcion=:descripcion AND id_tipo_proyecto NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }


        //editar tipo de proyecto
        public function editartp($datos){
            $consulta = 'UPDATE tipo_proyecto set descripcion = :descripcion
                        where id_tipo_proyecto = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar tipo proyecto
        public function eliminartp($datos){
            $consulta = 'DELETE FROM tipo_proyecto WHERE id_tipo_proyecto=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    }
    