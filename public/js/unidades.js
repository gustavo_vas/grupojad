$(document).ready(function(){
    $('#code').attr('disabled',true);
    $('#e').attr('disabled',true);

    //muestra el modal de agregar unidad
    $(document).on('click','#addunidad',function(){
        $('#cant').val('');
        $('#descripcion').val('');
        $('#canti').html('');
        $('#desc').html('');
        $('#addunidadmodal').modal('show');
    });

    //muestra modal para editar unidad
    $(document).on('click','.editunidadshow',function(){
        let id = parseInt($(this).parents("tr").find("td").eq(0).text());
        let descripcion = $(this).parents('tr').find('td').eq(1).text();
        let cant = $(this).parents('tr').find('td').eq(2).text();
        $('#code').val(id);
        $('#cant1').val(cant);
        $('#descripcion1').val(descripcion);
        $('#editunidadmodal').modal('show');
    });


    //agrega unidad
    $(document).on('click','#btnaddunidad',function(){
        if (document.getElementById('cant').value =='' && document.getElementById('descripcion').value=='') {
            $('#canti').html('* Este campo es obligatorio');
            $('#desc').html('* Este campo es obligatorio');
        }else if (document.getElementById('cant').value =='' && document.getElementById('descripcion').value!=''){
                $('#canti').html('* Este campo es obligatorio');
                $('#desc').html('');
        }else if (document.getElementById('cant').value !='' && document.getElementById('descripcion').value==''){
                $('#desc').html('* Este campo es obligatorio');
                $('#canti').html('');
        }else{
            let cant = $('#cant').val();
            let descripcion = $('#descripcion').val();
            let url = $('#ruta').val();

            let postData = {
                cant:cant,
                descripcion:descripcion
            };

            $.post(url+'/unidades/guardar',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Unidad Registrada Exitosamente');
                        $('#cant').val('');
                        $('#descripcion').val('');
                        $('#addunidadmodal').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('La unidad que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    //editar unidad
    $(document).on('click','#btneditunidad',function(){
        if (document.getElementById('cant1').value =='' && document.getElementById('descripcion1').value=='') {
            $('#canti1').html('* Este campo es obligatorio');
            $('#desc1').html('* Este campo es obligatorio');
        }else if (document.getElementById('cant1').value =='' && document.getElementById('descripcion1').value!=''){
                $('#canti1').html('* Este campo es obligatorio');
                $('#desc1').html('');
        }else if (document.getElementById('cant1').value !='' && document.getElementById('descripcion1').value==''){
                $('#desc1').html('* Este campo es obligatorio');
                $('#canti1').html('');
        }else{
            let cant = $('#cant1').val();
            let descripcion = $('#descripcion1').val();
            let code = $('#code').val();
            let url = $('#ruta').val();

            let postData = {
                cant:cant,
                descripcion:descripcion,
                code:code
            };

            $.post(url+'/unidades/update',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Unidad Registrada Exitosamente');
                        $('#cant1').val('');
                        $('#descripcion1').val('');
                        $('#editunidadmodal').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('La unidad que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    //muestra el modal para eliminar unidad
    $(document).on('click','.delete',function(){
        let idcuad = $(this).parents('tr').find('td').eq(0).text();
       $('#e').val(idcuad);
        $('#elimunidad').modal('show');
    });

    //elimina unidad
    $(document).on('click','#borrarunidad',function(){
        let url = $('#ruta').val();
        let idc = $('#e').val();
        postData = {
            idc:idc
        };
        
        $.post(url+'/unidades/eliminar',postData,function(response){
            if (response == '1') {                
                    toastr.success('Registro eliminado correctamente.');
                    $('#e').val('');
                    $('#elimunidad').modal('hide');
                    $('#okis').modal('show');
            }else{
                    toastr.error('Error al eliminar registro, verifique que no se este usado en otra tabla.');
            }
        });
    });
});