<?php
 class Devolucion
 {
    private $db;
    private $consulta;

    public function __construct(){
        $this->db = new Base;
    }
    public function __destruct(){
        $this->db = null;
    }

    public function getDevoluciones(){
        $consulta = "SELECT (d.id_devolucion)as code, (d.documento)as documento, 
                    ifnull(DATE_FORMAT(d.fecha_devuelto,'%d/%m/%Y'),'N/A')as fdevuelto
                    FROM devoluciones d
                    order by d.fecha_devuelto asc";

        $this->db->query($consulta);
        $resultado = $this->db->registros();
        return $resultado;
    }

    //metodo para agregar un maestro de entradas
    public function agregarDevolucion($datos){
        $dev = 'CALL adddevolucion(:doc, :fcreado, :fentrega, :usuario, :pentrega, :precibe)';
        $this->db->query($dev);

        $this->db->bind(':doc',$datos['documento']);
        $this->db->bind(':fcreado',$datos['fcreado']);
        $this->db->bind(':fentrega',$datos['fentrega']);
        $this->db->bind(':usuario',$datos['usuario']);
        $this->db->bind(':pentrega',$datos['pentrega']);
        $this->db->bind(':precibe',$datos['precibe']);

        $resultado = $this->db->registro();
        $iddevolucion = $resultado->id;
        return $iddevolucion;
    }

    public function devolverinventario($datos){
        $consulta = 'CALL devolverinventario(:idinv,:cantidad,:iddevolucion);';
        $this->db->query($consulta);

            //vincular los valores
        $this->db->bind(':idinv',$datos['idinv']);
        // $this->db->bind(':estado',$datos['estado']);
        $this->db->bind(':cantidad',$datos['cantidad']);
        $this->db->bind(':iddevolucion',$datos['iddevolucion']);

        if ($this->db->execute()) {
            return true;
        }else{
            return false;
        }
    }

    public function getdevolucion($id){
        $consulta = "SELECT (s.id_devolucion)as code, (s.documento)as documento, 
                    ifnull(DATE_FORMAT(s.fecha_devuelto,'%d/%m/%Y'),'N/A')as fdev,
                    (s.persona_devuelve)as pentrega, (s.persona_recibe)as precibe 
                    FROM devoluciones s
                    where s.id_devolucion=:id
                    order by s.fecha_devuelto asc";

        $this->db->query($consulta);
        $this->db->bind(':id',$id);
        $resultado = $this->db->registro();
        return $resultado;
    }

    public function getDetalle($id){
        $consulta = "SELECT (p.codigo)AS codigo, (p.nombre)AS producto,(p.descripcion)AS descripciono,
                    (p.tipo_producto)AS tipo, (dt.cantidad)AS cantidad  FROM detalle_devolucion dt
                    JOIN inventario inv ON dt.id_inventario=inv.id_inventario
                    JOIN productos p ON inv.id_producto=p.id_producto                        
                    WHERE id_devolucion=:id";

        $this->db->query($consulta);
        $this->db->bind(':id',$id);
        $resultado = $this->db->registros();
        return $resultado;
    }

    public function getDetallesalidas(){
        $consulta = "SELECT (dt.id_detalle_salida)AS idet,(p.codigo)AS codigo, (p.nombre)AS producto,
                    (p.descripcion)AS descripciono,
                    (p.tipo_producto)AS tipo, (dt.cantidad)AS cantidad, (dt.cant_instalado)AS instalado,
                    (dt.cant_devuelto)AS devuelto  
                    FROM detalle_salidas dt
                    JOIN inventario inv ON dt.id_inventario=inv.id_inventario
                    JOIN productos p ON inv.id_producto=p.id_producto";

        $this->db->query($consulta);
        $resultado = $this->db->registros();
        return $resultado;
    }
 }
 