<?php

    class Admin{
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function getcantproductos(){
            $consulta = 'SELECT * FROM productos';
            $this->db->query($consulta);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function getcantinventario(){
            $consulta = 'SELECT * FROM inventario';
            $this->db->query($consulta);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function getcantecnicos(){
            $consulta = 'SELECT * FROM tecnicos';
            $this->db->query($consulta);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function getcanproyectos(){
            $consulta = 'SELECT * FROM proyectos';
            $this->db->query($consulta);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function getcanentradas(){
            $consulta = 'SELECT * FROM entradas';
            $this->db->query($consulta);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function getcansalidas(){
            $consulta = 'SELECT * FROM salidas';
            $this->db->query($consulta);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function getcandev(){
            $consulta = 'SELECT * FROM devoluciones';
            $this->db->query($consulta);

            $resultado = $this->db->rowCount();
            return $resultado;
        }
    }