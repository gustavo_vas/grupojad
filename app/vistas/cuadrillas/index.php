<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.css">
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cuadrillas</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">                

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-sm-6">
                                            <h3 class="card-title float-sm-left">Datos de Cuadrillas</h3>  
                                        </div>
                                        <div class="col-sm-12">
                                            <button class="float-sm-right btn btn-success" id="addcuad" title="Agregar Cuadrilla">
                                                <i class="nav-icon fas fa-book"> 
                                                    <b>Agregar cuadrilla</b>
                                                </i>
                                            </button>
                                        </div><!-- /.col -->                         
                                    </div>
                                    
                                    <!-- /.card-header -->
                                    <div class="card-body table-responsive">
                                        <table id="example2" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre Cuadrilla</th>
                                                    <th>Encargado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($datos['cuadrillas'] as $cuadrilla) { ?>
                                                    <tr>
                                                        <td><?php echo $cuadrilla->id_cuadrilla; ?> </td>
                                                        <td><?php echo $cuadrilla->nombre_cuadrilla; ?></td>
                                                        <td><?php echo $cuadrilla->encargado_cuadrilla; ?></td>
                                                        <td>
                                                            <button title="Editar Cuadrilla" class="btn btn-primary editcuad"><i class='nav-icon fas fa-edit'></i></button>
                                                            <button title="Eliminar Cuadrilla" class="btn btn-danger delete"><i class='nav-icon fas fa-trash-alt'></i></button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>                            
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->

        <!-- modal para agregar cuadrilla  -->
        <div class="modal fade" id="addcuadrilla">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Cuadrillas</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                    <label for="cuadr" class="control-label" id="lblcuadr">Nombre</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="cuadr" required="true" placeholder="Ingresar cuadrilla">
                                    <label id="nom" style="color:red;"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="encarg" class="control-label" id="lblencarg">Encargado</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="encarg" required="true" placeholder="Ingresar encargado">
                                    <label id="enc" style="color:red;"></label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button id="btnaddcuadri" class="btn btn-primary">Guardar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <!-- modal para editar cuadrilla  -->
        <div class="modal fade" id="editcuadrilla">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Cuadrillas</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                                                    
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                    <label for="cuadr" class="control-label" id="lblcuadr">Codigo</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="code" required="true">
                                    <label id="code1" style="color:red;"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="cuadr" class="control-label" id="lblcuadr">Nombre</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="cuadr1" required="true" placeholder="Ingresar cuadrilla">
                                    <label id="nom1" style="color:red;"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="encarg" class="control-label" id="lblencarg">Encargado</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="encarg1" required="true" placeholder="Ingresar encargado">
                                    <label id="enc1" style="color:red;"></label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button id="btneditcuadri" class="btn btn-primary">Modificar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <!-- modal para confirmar eliminacion  -->
        <div class="modal fade" id="okis">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 id="encab">Transacción realizada correctamente</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <a href="<?php echo RUTA_URL; ?>/cuadrillas/" class="btn btn-success form-control" id="ok">OK</a>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <!-- <button class="btn btn-danger" data-dismiss="modal">CERRAR</button> -->
                            <!-- <button class="btn btn-primary" id="ok">Aceptar</button> -->
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <!-- modal para eliminar cliente  -->
        <div class="modal fade" id="elimcuad">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="encab">Formulario de confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta1" value="<?php echo RUTA_URL;?>" readonly>
                        <input type="hidden" id="e">
                        <div class="modal-body justify-center">
                            <h3>¿Desea eliminar este registro?</h3>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button class="btn btn-danger" data-dismiss="modal">CERRAR</button>
                            <button class="btn btn-primary" id="borrarcuad">Aceptar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>   

        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/cuadrillas.js"></script>
        <script src="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.js"></script>

        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                });
            });
        </script>
    </body>
</html>