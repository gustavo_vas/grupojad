$(document).ready(function(){
    $('#code').attr('disabled',true);
    $('#e').attr('disabled',true);

    //muestra el modal de agregar marca
    $(document).on('click','#addmarca',function(){
        $('#marca').val('');
        $('#descripcion').val('');
        $('#marc').html('');
        $('#desc').html('');
        $('#addmarc').modal('show');
    });

    //muestra modal para editar marca
    $(document).on('click','.editmarcashow',function(){
        let id = parseInt($(this).parents("tr").find("td").eq(0).text());
        let nombre = $(this).parents('tr').find('td').eq(1).text();
        let descripcion = $(this).parents('tr').find('td').eq(2).text();
        $('#code').val(id);
        $('#marca1').val(nombre);
        $('#descripcion1').val(descripcion);
        $('#editmarca').modal('show');
    });


    //agrega marca
    $(document).on('click','#btnaddmarca',function(){
        if (document.getElementById('marca').value =='' && document.getElementById('descripcion').value=='') {
            $('#marc').html('* Este campo es obligatorio');
            $('#desc').html('* Este campo es obligatorio');
        }else if (document.getElementById('marca').value =='' && document.getElementById('descripcion').value!=''){
                $('#marc').html('* Este campo es obligatorio');
                $('#desc').html('');
        }else if (document.getElementById('marca').value !='' && document.getElementById('descripcion').value==''){
                $('#desc').html('* Este campo es obligatorio');
                $('#marc').html('');
        }else{
            let marca = $('#marca').val();
            let descripcion = $('#descripcion').val();
            let url = $('#ruta').val();

            let postData = {
                marca:marca,
                descripcion:descripcion
            };

            $.post(url+'/marcas/guardar',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Marca Registrada Exitosamente');
                        $('#marca').val('');
                        $('#descripcion').val('');
                        $('#addmarc').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('La Marca que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    //editar marca
    $(document).on('click','#btneditmarca',function(){
        if (document.getElementById('marca1').value =='' && document.getElementById('descripcion1').value=='') {
            $('#marc1').html('* Este campo es obligatorio');
            $('#desc1').html('* Este campo es obligatorio');
        }else if (document.getElementById('marca1').value =='' && document.getElementById('descripcion1').value!=''){
                $('#marc1').html('* Este campo es obligatorio');
                $('#desc1').html('');
        }else if (document.getElementById('marca1').value !='' && document.getElementById('descripcion1').value==''){
                $('#desc1').html('* Este campo es obligatorio');
                $('#marc1').html('');
        }else{
            let marca = $('#marca1').val();
            let descripcion = $('#descripcion1').val();
            let code = $('#code').val();
            let url = $('#ruta').val();

            let postData = {
                marca:marca,
                descripcion:descripcion,
                code:code
            };

            $.post(url+'/marcas/update',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Marca Registrada Exitosamente');
                        $('#marca1').val('');
                        $('#descripcion1').val('');
                        $('#editmarca').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('La Marca que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    $(document).on('click','.delete',function(){
        let idcuad = $(this).parents('tr').find('td').eq(0).text();
       $('#e').val(idcuad);
        $('#elimmarca').modal('show');
    });

    $(document).on('click','#borrarmarca',function(){
        let url = $('#ruta').val();
        let idc = $('#e').val();
        postData = {
            idc:idc
        };
        
        $.post(url+'/marcas/eliminar',postData,function(response){
            if (response == '1') {                
                    toastr.success('Registro eliminado correctamente.');
                    $('#e').val('');
                    $('#elimmarca').modal('hide');
                    $('#okis').modal('show');
            }else{
                    toastr.error('Error al eliminar registro, recargue la pagina e intente de nuevo.');
            }
        });
    });
});