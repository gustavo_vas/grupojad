<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalle de entrada</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">

                        <div class="jumbotron">
                            <h3 class="display-6">No. Documento <?php echo $datos['documento']; ?></h3>
                            <p class="lead">Fecha de entrada <?php echo $datos['fentrada']; ?></p>
                            <hr class="my-4">
                            <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-sm table-hover" id="t">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Producto</th>
                                                <th>Descripción</th>
                                                <th>Tipo</th>
                                                <th>Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody id="table-detalle">
                                            <?php $total=0; foreach($datos['detalle'] as $detalle){
                                                $total += $detalle->cantidad;
                                                if ($detalle->tipo == 1) {
                                                    $tipo = 'Seriado';
                                                }else{
                                                    $tipo = 'No Seriado';
                                                }

                                                echo '<tr>';
                                                    echo '<td>'.$detalle->codigo.'</td>';
                                                    echo '<td>'.$detalle->producto.'</td>';
                                                    echo '<td>'.$detalle->descripciono.'</td>';
                                                    echo '<td>'.$tipo.'</td>';
                                                    echo '<td>'.$detalle->cantidad.'</td>';
                                                echo '</tr>';
                                            }?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td style="text-align: right;" colspan="4"><b>TOTAL</b></td>
                                                <td id="total"><?php echo $total; ?></td>
                                            </tr>
                                        <tfoot>
                                    </table>
                                </div>
                            <a class="btn btn-primary btn-lg" href="<?php echo RUTA_URL; ?>/entradas/" role="button">Regresar</a>
                        </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#t').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                });
            });
        </script>
    </body>
</html>