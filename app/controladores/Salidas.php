<?php
    class Salidas extends Controlador
    {
        public function __construct(){
            Sesion::start();
            $this->salidaModelo = $this->modelo('Salida');
            // $this->productoModelo = $this->modelo('Producto');
            $this->proyectoModelo = $this->modelo('Proyecto');
            $this->inventarioModelo = $this->modelo('Inventario');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $salidas = $this->salidaModelo->getSalidas();
                $datos = [
                    'salidas' => $salidas
                ];
                $this->vista('/salidas/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function crear(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $proyectos = $this->proyectoModelo->getproyectos();
                $inventarios = $this->inventarioModelo->getinventarios();
                $datos = [
                    'proyectos' => $proyectos,
                    'inventarios'=>$inventarios
                ];
                $this->vista('/salidas/crear',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function guardar(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $documento = $_POST['documento'];
                    $fentrega = $_POST['fentrega'];
                    $proyectos = $_POST['proyectos'];
                    $pentrega = $_POST['pentrega'];
                    $precibe = $_POST['precibe'];
                    $fcreado = date('Y-m-d');
                    $usuario = Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser');
                    $detalles = json_decode($_POST['detalle']);
                    // $estado = 6;//hacer dinamico este valor

                    $datos = [
                        'documento'=>$documento,
                        'fentrega'=>$fentrega,
                        'proyectos' => $proyectos,
                        'pentrega'=>$pentrega,
                        'precibe'=>$precibe,
                        'fcreado'=>$fcreado,
                        'usuario'=>$usuario
                    ];

                    $idsalida = $this->salidaModelo->agregarSalida($datos);
                    if($idsalida){
                        foreach ($detalles as $det) {
                            $detalle = [
                                'idinv' => $det->id,
                                'cantidad' => $det->cant,
                                'idsalida' => $idsalida
                                // 'estado' => $estado
                            ];
                            // echo $identrada;
                            if($this->salidaModelo->salidainventario($detalle)){
                                echo '1';//ingresado
                            }else{
                                echo '2';//error
                            }
                        }
                    }else{
                        echo 'no';
                    }
                    // echo json_encode($detalles);
                }else{
                    redireccionar('/entradas/crear');
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function show($id){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if (is_numeric($id)) {
                    $salida = $this->salidaModelo->getSalida($id);
                    if ($salida->documento != '') {
                        $detalle = $this->salidaModelo->getDetalle($id);
                        $datos = [
                            'documento'=>$salida->documento,
                            'fentrega'=>$salida->fentrega,
                            'nproy'=>$salida->nproy,
                            'pentrega'=>$salida->pentrega,
                            'precibe'=>$salida->precibe,
                            'detalle'=>$detalle
                        ];
                        $this->vista('/salidas/show',$datos);
                    }else{
                        redireccionar('/errores/notfound');
                    }  
                    
                }else{
                    redireccionar('/errores/notfound');
                }                
            }else{
                redireccionar('/errore/destroySesion');
            }
        }

        public function reportes($id){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if (is_numeric($id)) {
                    $salida = $this->salidaModelo->getSalida($id);
                    if ($salida->documento != '') {
                        $detalle = $this->salidaModelo->getDetalle($id);
                        $datos = [
                            'documento'=>$salida->documento,
                            'fentrega'=>$salida->fentrega,
                            'nproy'=>$salida->nproy,
                            'nomcuads'=>$salida->nomcuads,
                            'pentrega'=>$salida->pentrega,
                            'precibe'=>$salida->precibe,
                            'detalle'=>$detalle
                        ];
                        $this->vista('/salidas/informe',$datos);
                    }else{
                        redireccionar('/errores/notfound');
                    }  
                    
                }else{
                    redireccionar('/errores/notfound');
                }                
            }else{
                redireccionar('/errore/destroySesion');
            }
        }

        public function instalaciones(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $salidas = $this->salidaModelo->getSalidas();
                $datos = [
                    'salidas' => $salidas
                ];
                $this->vista('/salidas/instalaciones',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function instalar($id){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if (is_numeric($id)) {
                    $salida = $this->salidaModelo->getSalida($id);
                    if ($salida->documento != '') {
                        $detalle = $this->salidaModelo->getDetalle($id);
                        $datos = [
                            'documento'=>$salida->documento,
                            'fentrega'=>$salida->fentrega,
                            'nproy'=>$salida->nproy,
                            'pentrega'=>$salida->pentrega,
                            'precibe'=>$salida->precibe,
                            'detalle'=>$detalle
                        ];
                        $this->vista('/salidas/instalar',$datos);
                    }else{
                        redireccionar('/errores/notfound');
                    }  
                    
                }else{
                    redireccionar('/errores/notfound');
                }                
            }else{
                redireccionar('/errore/destroySesion');
            }
        }

        public function instalarproducto(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $iddeta = $_POST['codeid'];
                    $cinstall = $_POST['cantinstal'];

                    $datos = [
                        'detid'=>$iddeta,
                        'instalcant'=>$cinstall
                    ];
                    if($this->salidaModelo->addinstall($datos)){
                        echo '1';//ingresado
                    }else{
                        echo '2';//error
                    }
                }else{
                    redireccionar('/salidas/instalar');
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function reportesins($id){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if (is_numeric($id)) {
                    $salida = $this->salidaModelo->getSalida($id);
                    if ($salida->documento != '') {
                        $detalle = $this->salidaModelo->getDetalle($id);
                        $datos = [
                            'documento'=>$salida->documento,
                            'fentrega'=>$salida->fentrega,
                            'nproy'=>$salida->nproy,
                            'nomcuads'=>$salida->nomcuads,
                            'pentrega'=>$salida->pentrega,
                            'precibe'=>$salida->precibe,
                            'detalle'=>$detalle
                        ];
                        $this->vista('/salidas/informeins',$datos);
                    }else{
                        redireccionar('/errores/notfound');
                    }  
                    
                }else{
                    redireccionar('/errores/notfound');
                }                
            }else{
                redireccionar('/errore/destroySesion');
            }
        }
    }
