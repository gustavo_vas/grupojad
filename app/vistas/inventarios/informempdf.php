<?php
// page_protect();
$this->protegerPagina();
date_default_timezone_set('America/El_Salvador');
require_once RUTA_APP."/vistas/include/header.php";
// require_once RUTA_APP."/vistas/include/footer.php";
// require_once RUTA_APP."/vistas/include/navadmin.php";
// $mpdf= new mPDF('c', 'A4');
if(Sesion::getSesion('tipo')==1){
    $tp= 'Administrador';
  }else{
    $tp= 'Normal';
  }

//   <h4 class="name">'.Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser').'</h4>

    $html='<hr>
    <div id="company">
        <h3 class="name">GRUPO JAD EL SALVADOR S.A. DE C.V.</h3>
        <div>Final 25 Av. Sur, Barrio San Rafael, Santa Ana, Santa Ana.</div>
        <div>(503) 2441-0393</div>
        <div><a href="mailto:grupojad@grupojad.net">grupojad@grupojad.net</a></div>
    </div><hr>
    <div id="details" class="clearfix" style="display: grid; grid-template-columns: repeat(3, 1fr); grid-auto-rows: minmax(80px, auto); grid-gap: 15px;">
        <div id="client" style="padding-left: 6px; border-left: 6px solid #0087C3; float: left;">
          <div class="to">Creado por:<b> '.Sesion::getSesion('nombreUser').' '. Sesion::getSesion('apellidoUser').'</b></div>
           
          <!-- <div class="address">796 Silver Harbour, TX 79273, US</div> -->
          <div class="address">'.$tp.'</div>
          <div class="email"><a href="mailto:john@example.com">'.Sesion::getSesion('correo').'</a></div>
        </div>
        <div id="invoice" style="float: right; text-align: right;">
          <h3>INVENTARIO '.$datos['tipo'].'</h3>
          <div class="date">Fecha de creación: '.date('d/m/Y').'</div>
          <!-- <div class="date">Due Date: 30/06/2014</div> -->
        </div>
      </div>
    <div class="card-body">
        <table style="border-collapse:collapse; border: none; font-size: 75%;" width="100%" border="1" CELLPADDING="5">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Producto</th>
                    <th>Descripción</th>
                    <th>tipo</th>
                    <th>Existencias </th>
                    <th>En Ruta </th>
                    <th>Instalado </th>
                </tr>
            </thead>
            <tbody>';
                foreach ($datos['inventarios'] as $producto) { 
                    if ($producto->tipo == 1) {
                        $tipo = 'Seriado';
                    }else{
                        $tipo = 'No Seriado';
                    } 
                    $html.='<tr inde="<?php echo $producto->code; ?>">
                        <td>'.$producto->codigo.' </td>
                        <td>'.$producto->nombre.'</td>
                        <td>'.$producto->descripcion.'</td>
                        <td>'.$tipo.'</td>
                        <td>'.$producto->cantidad.'</td>
                        <td>'.$producto->cantidadruta.'</td>
                        <td>'.$producto->cantidadinstalado.'</td>
                    </tr>';
                }
            $html.='</tbody>                            
        </table>
    </div>
                                    
'; 
$html .='';

$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML($html);
$mpdf->Output();

?>