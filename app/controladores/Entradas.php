<?php
    class Entradas extends Controlador
    {
        public function __construct(){
            Sesion::start();
            $this->entradaModelo = $this->modelo('Entrada');
            $this->productoModelo = $this->modelo('Producto');
            $this->estadoModelo = $this->modelo('Estado');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $entradas = $this->entradaModelo->getEntradas();
                $datos = [
                    'entradas' => $entradas
                ];
                $this->vista('/entradas/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function crear(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $productos = $this->productoModelo->getproductos();
                $estados = $this->estadoModelo->getEstados();
                $datos = [
                    'productos' => $productos,
                    'estados'=>$estados
                ];
                $this->vista('/entradas/crear',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function guardar(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $documento = $_POST['documento'];
                    $fentrada = $_POST['fentrada'];
                    $fcreado = date('Y-m-d');
                    $usuario = Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser');
                    $detalles = json_decode($_POST['detalle']);
                    // $estado = $_POST['estado'];//hacer dinamico este valor

                    $datos = [
                        'documento'=>$documento,
                        'fentrada'=>$fentrada,
                        'fcreado'=>$fcreado,
                        'usuario'=>$usuario
                    ];

                    $identrada = $this->entradaModelo->agregarEntrada($datos);
                    if($identrada){
                        foreach ($detalles as $det) {
                            $detalle = [
                                'idprod' => $det->id,
                                'cantidad' => $det->cant,
                                'identrada' => $identrada
                                // 'estado' => $estado
                            ];
                            // echo $identrada;
                            if($this->entradaModelo->agregarinventario($detalle)){
                                echo '1';//ingresado
                            }else{
                                echo '2';//error
                            }
                        }
                    }else{
                        echo 'no';
                    }
                    // echo json_encode($detalles);
                }else{
                    redireccionar('/entradas/crear');
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function show($id){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if (is_numeric($id)) {
                    $entrada = $this->entradaModelo->getEntrada($id);
                    if ($entrada->documento != '') {
                        $detalle = $this->entradaModelo->getDetalle($id);
                        $datos = [
                            'documento'=>$entrada->documento,
                            'fentrada'=>$entrada->fentrada,
                            'detalle'=>$detalle
                        ];
                        $this->vista('/entradas/show',$datos);
                    }else{
                        redireccionar('/errores/notfound');
                    }  
                    
                }else{
                    redireccionar('/errores/notfound');
                }                
            }else{
                redireccionar('/errore/destroySesion');
            }
        }
        
    }
    