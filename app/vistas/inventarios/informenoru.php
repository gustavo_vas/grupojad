<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Reportes</title>
    <!-- <?php //require_once RUTA_APP."/vistas/include/header.php"; ?> -->
    <link rel="stylesheet" href="<?php echo RUTA_URL;?>/css/style.css" media="all" />
  </head>
  <body>    
    <header class="clearfix">
      <hr>
      <div id="logo">
        <img src="<?php echo RUTA_URL;?>/img/logo1.jpg">
      </div>
      <div id="company">
        <h2 class="name">GRUPO JAD EL SALVADOR S.A. DE C.V.</h2>
        <div>Final 25 Av. Sur, Barrio San Rafael, Santa Ana, Santa Ana.</div>
        <div>(503) 2441-0393</div>
        <div><a href="mailto:grupojad@grupojad.net">grupojad@grupojad.net</a></div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">Creado por:</div>
          <h2 class="name"><?php echo Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser'); ?></h2>
          <!-- <div class="address">796 Silver Harbour, TX 79273, US</div> -->
          <div class="address"><?php
            if(Sesion::getSesion('tipo')==1){
              echo 'Administrador';
            }else{
              echo 'Normal';
            }
           ?></div>
          <div class="email"><a href="mailto:john@example.com"><?php echo Sesion::getSesion('correo'); ?></a></div>
        </div>
        <div id="invoice">
          <h1>INVENTARIO <?php echo $datos['tipo']; ?></h1>
          <div class="date">Fecha de creación: <?php echo date('d/m/Y'); ?></div>
          <!-- <div class="date">Due Date: 30/06/2014</div> -->
        </div>
      </div>
      
      <div class="card-body">
        <table id="example" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th><b>CODIGO</b></th>
                    <th><b>PRODUCTO</b></th>
                    <th><b>DESCRIPCION</b></th>
                    <!-- <th>Marca </th> -->
                    <th><b>TIPO PROD.</b></th>
                    <th><b>UNIDAD</b></th>
                    <th><b>BODEGA</b></th>
                    <th><b>RUTA</b></th>
                    <!-- <th>Acciones</th> -->
                </tr>
            </thead>
            <tbody>
                <?php foreach ($datos['inventarios'] as $producto) { ?>
                    <tr inde="<?php echo $producto->code; ?>">
                        <td style="border: 1px solid black;"><?php echo $producto->codigo; ?> </td>
                        <td style="border: 1px solid black;"><?php echo $producto->nombre; ?></td>
                        <td style="border: 1px solid black;"><?php echo $producto->descripcion; ?></td>
                        <!-- <td><?php //echo $producto->marca; ?></td>                                                         -->
                        <td style="border: 1px solid black;">                                                             
                            <?php
                                if ($producto->tipo == 1) {
                                    echo 'Seriado';
                                }else{
                                    echo 'No Seriado';
                                } 
                              ?>
                        </td>
                        <td style="border: 1px solid black;"><?php echo $producto->unidad; ?></td>
                        <td class="centrado" style="border: 1px solid black;"><?php echo $producto->cantidad; ?></td>
                        <td class="centrado" style="border: 1px solid black;"><?php echo $producto->cantidadruta; ?></td>
                    </tr>
                <?php } ?>
            </tbody>                            
        </table>
    </div>
    <!-- /.card-body -->

      <div id="thanks"></div>
      <div id="notices">
        <div></div>
        <div class="notice">GRUPO JAD EL SALVADOR S.A. DE C.V.</div>
      </div><br>
    </main>
    <!-- <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer> -->
  </body>
</html>