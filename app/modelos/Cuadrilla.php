<?php
    class Cuadrilla
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }

        public function __destruct(){
            $this->db = null;
        }
 
        //obtener cuadrillas
        public function obtenercuadrillas(){
            $consulta = 'SELECT * FROM cuadrillas order by nombre_cuadrilla asc';
            $this->db->query($consulta);

            $resultado = $this->db->registros();
            return $resultado;
        }

        //obtener cantidad cuadrillas por nombre
        public function obtenercantcuadrillanombre($nombre){
            $consulta = 'SELECT * FROM cuadrillas WHERE nombre_cuadrilla=:nombre';
            $this->db->query($consulta);

            $this->db->bind(':nombre',$nombre);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function obtenercantcuadrillanombrenotid($nombre,$id){
            $consulta = 'SELECT * FROM cuadrillas 
                        WHERE nombre_cuadrilla=:nombre AND id_cuadrilla NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':nombre',$nombre);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //guardar cuadrilla
        public function agregarCuadrilla($datos){
            $consulta = 'INSERT INTO cuadrillas (nombre_cuadrilla,encargado_cuadrilla) 
                    values(:nombre,:encargado)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':encargado',$datos['encargado']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //editar cuadrilla
        public function editarCuadrilla($datos){
            $consulta = 'UPDATE cuadrillas set nombre_cuadrilla = :nombre,encargado_cuadrilla = :encargado
                        where id_cuadrilla = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':encargado',$datos['encargado']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar cuadrilla
        public function eliminarCuadrilla($datos){
            $consulta = 'DELETE FROM cuadrillas WHERE id_cuadrilla=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
        
    }
    