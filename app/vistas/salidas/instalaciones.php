<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.css">
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Instalaciones</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">                

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-sm-6">
                                            <h3 class="card-title float-sm-left">Instalación de material</h3>  
                                        </div>
                                        <!-- <div class="col-sm-12">
                                            <a href="<?php //echo RUTA_URL; ?>/salidas/crear" class="float-sm-right btn btn-success">
                                                <i class="nav-icon fas fa-user-plus"> 
                                                    <b>Agregar Entrega</b>
                                                </i>
                                            </a>
                                        </div> -->
                                        <!-- /.col -->                         
                                    </div>
                                    
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="example2" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Documento</th>
                                                    <th>Num. Proyecto</th>
                                                    <th>Cuadrilla</th>
                                                    <th>Fecha entrega</th>
                                                    <th>fecha actualizado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($datos['salidas'] as $salida) { ?>
                                                    
                                                    <tr inde="<?php echo $salida->code; ?>">
                                                        <td><?php echo $salida->documento; ?> </td>
                                                        <td><?php echo $salida->numproyecto; ?> </td>
                                                        <td><?php echo $salida->nomcuads; ?> </td>
                                                        <td><?php echo $salida->fentrega; ?></td>
                                                        <td><?php echo $salida->fupdate; ?></td>
                                                        <td>
                                                            <a href="<?php echo RUTA_URL; ?>/salidas/instalar/<?php echo $salida->code; ?>" title="Ver entrada" class="btn btn-secondary"><i class='nav-icon fas fa-cogs'></i></a>
                                                            <a href="<?php echo RUTA_URL; ?>/salidas/reportesins/<?php echo $salida->code; ?>" target="_blank" title="Imprimir Informe" class="btn btn-secondary"><i class='nav-icon fas fa-print'></i></a>
                                                            <!-- <a href="<?php //echo RUTA_URL; ?>/entradas/editar/<?php //echo $entrada->code; ?>" title="Editar entrada" class="btn btn-primary"><i class='nav-icon fas fa-edit'></i></a> -->
                                                            <!-- <button title="Eliminar entrada" class="btn btn-danger delete"><i class='nav-icon fas fa-trash-alt'></i></button> -->
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>                            
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>

        <!-- modal para eliminar cliente  -->
        <div class="modal fade" id="eliminar">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="encab">Formulario de confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta1" value="<?php echo RUTA_URL;?>" readonly>
                        <input type="hidden" id="e">
                        <div class="modal-body justify-center">
                            <h3>¿Desea eliminar este registro?</h3>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button class="btn btn-danger" data-dismiss="modal">CERRAR</button>
                            <button class="btn btn-primary" id="borrarprod">Aceptar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>   

         <!-- modal para confirmar eliminacion  -->
         <div class="modal fade" id="okis">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 id="encab">Transacción realizada correctamente</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <a href="<?php echo RUTA_URL; ?>/entradas/" class="btn btn-success form-control" id="ok">OK</a>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <!-- <button class="btn btn-danger" data-dismiss="modal">CERRAR</button> -->
                            <!-- <button class="btn btn-primary" id="ok">Aceptar</button> -->
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/salidas.js"></script>
        <script src="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.js"></script>

        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                });
            });
        </script>
    </body>
</html>