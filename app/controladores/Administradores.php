<?php
    class Administradores extends Controlador{

        public function __construct(){
            date_default_timezone_set('America/El_Salvador'); 
            $this->adminModelo = $this->modelo('Admin');
            Sesion::start();
        }
        
        public function index(){            
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                $cantproductos = $this->adminModelo->getcantproductos();
                $inv = $this->adminModelo->getcantinventario();
                $tect = $this->adminModelo->getcantecnicos();
                $proy = $this->adminModelo->getcanproyectos();
                $entradas = $this->adminModelo->getcanentradas();
                $salidas = $this->adminModelo->getcansalidas();
                $dev = $this->adminModelo->getcandev();
                $datos = [
                    'cantidad'=>$cantproductos,
                    'inv'=>$inv,
                    'tect'=>$tect,
                    'proy'=>$proy,
                    'entrada'=>$entradas,
                    'salida'=>$salidas,
                    'dev'=>$dev
                ];
                $this->vista('/administrador/index',$datos);
            }else{
                redireccionar('/login/destroySesion');
            }
        }
        
        //metodo para eliminar las sesiones
        public function destroySesion(){
            Sesion::destroy();
            header('Location: '.RUTA_URL);
        }
    }