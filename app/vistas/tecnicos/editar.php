<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Actualización de Técnicos</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5">
                                <form action="<?php echo RUTA_URL; ?>/tecnicos/update/<?php echo $datos['id']; ?>" method="post">
                                    <div class="form-group row">
                                        <label for="nombres" class="col-sm-3">Nombre</label>
                                        <input type="text" class="form-control col-sm-9" name="nombres" placeholder="Ingresar nombre" required="true" 
                                            value="<?php echo $datos['nombre']; ?>">    
                                    </div>

                                    <div class="form-group row">
                                        <label for="direccion" class="col-sm-3">Dirección</label>
                                        <input type="text" class="form-control col-sm-9" name="direccion" placeholder="Ingresar dirección" required="true" 
                                            value="<?php echo $datos['dir']; ?>">  
                                    </div>

                                <div class="form-group row">
                                    <label for="correo" class="col-sm-3">Correo</label>
                                    <input type="email" class="form-control col-sm-9" name="correo" placeholder="Ingresar Correo" required="true" 
                                        value="<?php echo $datos['email']; ?>"> 
                                </div>

                                <div class="form-group row">
                                    <label for="telefono" class="col-sm-3">Telefono</label>
                                    <input type="text" class="form-control col-sm-9" name="telefono" placeholder="Ingresar telefono" required="true" 
                                        value="<?php echo $datos['tel']; ?>">	    
                                </div>

                                <div class="form-group row">
                                    <label for="dui" class="col-sm-3">DUI</label>
                                    <input type="text" class="form-control col-sm-9" name="dui" placeholder="Ingresar DUI" required="true"
                                        value="<?php echo $datos['dui']; ?>">	    
                                </div>

                                <div class="form-group row">
                                    <label for="cuad" class="col-sm-3">Cuadrilla</label>
                                    <select class="form-control col-sm-9" name="cuad" required="true">
                                        <option value="<?php echo $datos['codecuadrilla']; ?>"><?php echo $datos['cuadrilla']; ?></option>
                                        <?php 
                                            foreach ($datos['cuadrillas'] as $cuadrilla) {
                                                echo '<option value="'.$cuadrilla->id_cuadrilla.'">'.$cuadrilla->nombre_cuadrilla.'</option>';
                                            }
                                        ?>
                                    </select>    
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Guardar cambios</button>
                                <button type="reset" class="btn btn-info">Resetear formulario</button>
                                <a href="<?php echo RUTA_URL; ?>/tecnicos/">
                                    <button type="button" class="btn btn-danger">Cancelar</button>
                                </a>
                                </form>
                            </div>
                        </div>
                    </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
    </body>
</html>