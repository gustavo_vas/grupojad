<?php
    class Unidad
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //obtener unidades
        public function getUnidades(){
            $consulta = 'SELECT * FROM unidades order by descripcion';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //obtener cantidad unidades por descripcion
        public function obtenercantunidaddesc($descripcion){
            $consulta = 'SELECT * FROM unidades WHERE descripcion=:descripcion';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad unidades por descripcion para editar
        public function obtenercantunidaddescnotid($descripcion,$id){
            $consulta = 'SELECT * FROM unidades 
                        WHERE descripcion=:descripcion AND id_unidad NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //guardar unidad
        public function agregarunidad($datos){
            $consulta = 'INSERT INTO unidades (cant_granel,descripcion) 
                    values(:cant,:descripcion)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cant',$datos['cant']);
            $this->db->bind(':descripcion',$datos['descripcion']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //editar unidad
        public function editarunidad($datos){
            $consulta = 'UPDATE unidades set cant_granel = :cant,descripcion = :descripcion
                        where id_unidad = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cant',$datos['cant']);
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar unidad
        public function eliminarunidad($datos){
            $consulta = 'DELETE FROM unidades WHERE id_unidad=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    }
    