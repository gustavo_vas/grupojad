<?php
    class Entrada
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        public function getEntradas(){
            $consulta = "SELECT (e.id_entrada)as code, (e.documento)as documento, ifnull(DATE_FORMAT(e.fecha_entrada,'%d/%m/%Y'),'N/A')as fentrada, 
                        ifnull(DATE_FORMAT(e.fecha_actualizado,'%d/%m/%Y'),'N/A')as fupdate FROM entradas e
                        order by e.fecha_entrada asc";

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function getEntrada($id){
            $consulta = "SELECT (e.id_entrada)as code, (e.documento)as documento, ifnull(DATE_FORMAT(e.fecha_entrada,'%d/%m/%Y'),'N/A')as fentrada, 
                        ifnull(DATE_FORMAT(e.fecha_actualizado,'%d/%m/%Y'),'N/A')as fupdate FROM entradas e
                        where e.id_entrada=:id
                        order by e.fecha_entrada asc";

            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        public function getDetalle($id){
            $consulta = "SELECT (p.codigo)AS codigo, (p.nombre)AS producto,(p.descripcion)AS descripciono,
                        (p.tipo_producto)AS tipo, (dt.cantidad)AS cantidad  FROM detalle_entradas dt
                        JOIN productos p ON dt.id_producto=p.id_producto
                        WHERE id_entrada=:id";

            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //metodo para agregar un maestro de entradas
        public function agregarEntrada($datos){
            $dev = 'CALL addentrada(:doc, :fcreado, :fentrada, :usuario)';
            $this->db->query($dev);

            $this->db->bind(':doc',$datos['documento']);
            $this->db->bind(':fcreado',$datos['fcreado']);
            $this->db->bind(':fentrada',$datos['fentrada']);
            $this->db->bind(':usuario',$datos['usuario']);

            $resultado = $this->db->registro();
            $identrada = $resultado->id;
            return $identrada;
        }

        public function agregarDetalleEntrada($datos){
            $consulta = 'INSERT INTO detalle_entradas (id_entrada, id_producto, cantidad) 
            values(:identrada, :idproducto, :cantidad)';
            $this->db->query($consulta);

                //vincular los valores
            $this->db->bind(':identrada',$datos['identrada']);
            $this->db->bind(':idproducto',$datos['idprod']);
            $this->db->bind(':cantidad',$datos['cantidad']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function agregarinventario($datos){
            $consulta = 'CALL addinventariodeentrada(:idproducto,:cantidad,:identrada);';
            $this->db->query($consulta);

                //vincular los valores
            $this->db->bind(':idproducto',$datos['idprod']);
            // $this->db->bind(':estado',$datos['estado']);
            $this->db->bind(':cantidad',$datos['cantidad']);
            $this->db->bind(':identrada',$datos['identrada']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
        
    }
    