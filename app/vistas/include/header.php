    <meta name='Autor' content='Ing. Gustavo Vasquez'>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="id=edge">
    <title><?php echo NOMBRESITIO; ?></title>
    <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  
 
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/datatables/dataTables.bootstrap4.css">
