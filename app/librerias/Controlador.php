<?php
    //clase controlador pricipal que permite cargar las vistas y los modelos
    class Controlador{

        public function __construct(){
            Sesion::start();
        }
        //cargar modelos
        public function modelo($modelo){
            //requerir los modelos
            require_once '../app/modelos/'.$modelo.'.php';

            //instanciar el modelo y retornar instancia
            return new $modelo();
        }

        //cargar vistas
        public function vista($vista,$datos=[]){
            //verificar si existe la vista
            if (file_exists('../app/vistas/'.$vista.'.php')) {
                // requerir vistas
                require_once '../app/vistas/'.$vista.'.php';
            }else{
                // require_once '../app/vistas/errores/notfound.php';
                redireccionar('/errores/');
                // die('La vista especificada no fue encontrada el el directorio');
            }
        }

        public function protegerPagina(){
            @session_start();
            if (isset($_SESSION['HTTP_USER_AGENT'])) {
                    if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT'])) {
                        @session_destroy();
                        header('Location: '.RUTA_URL);
                        exit();
                    }
                }

                if (!isset($_SESSION['codusuario']) && !isset($_SESSION['usuario']) && !isset($_SESSION['estado'])) {
                    @session_destroy();
                    header('Location: '.RUTA_URL);
                    exit();
                } else {
                    
            }
        }
    }