<?php
    class Marcas extends Controlador
    {
        public function __construct(){
            Sesion::start();
            $this->marcaModelo = $this->modelo('Marca');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $marcas = $this->marcaModelo->getMarcas();
                $datos = [
                    'marcas'=>$marcas
                ];
                $this->vista('/marcas/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $marca = $_POST['marca'];
                    $marcas = $this->marcaModelo->obtenercantmarcanombre($marca);

                    if ($marcas==0) {
                        $datos = [
                            'marca' => $_POST['marca'],
                            'descripcion' => $_POST['descripcion']
                        ];
    
                        if($this->marcaModelo->agregarmarca($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function update(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $marca = $_POST['marca'];
                    $id = $_POST['code'];
                    $marcas = $this->marcaModelo->obtenercantmarcanombrenotid($marca,$id);

                    if ($marcas==0) {
                        $datos = [
                            'marca' => $_POST['marca'],
                            'descripcion' => $_POST['descripcion'],
                            'code' => $_POST['code']
                        ];
    
                        if($this->marcaModelo->editarMarca($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function eliminar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $datos = [
                            'code' => $_POST['idc']
                        ];
    
                        if($this->marcaModelo->eliminarMarca($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }                  
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    