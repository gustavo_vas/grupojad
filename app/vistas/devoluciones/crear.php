<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>

        <style>
            .centrado{
                margin-left: 5%; 
                width: 100%;
            }
        </style>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-5 centrado">
                    <h1 class="m-0 text-dark">Devolución de materiales</h1>
                </div><!-- /.col -->
                
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">salidas</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-11">
                    <div class="card centrado">
                        <div class="card-header">
                            <!-- <form id="compras-form">  -->
                                <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                <div class="form-group row border-1">                                    
                                    <div class="col-sm-2">
                                        <input type="number" class="form-control" id="doc" name="doc" placeholder="Documento" required="true">
                                        <label id="dc" style="color:red;"></label>
                                    </div> 

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Fecha" value="<?php echo date("Y-m-d"); ?>">
                                        <label id="fec" style="color:red;"></label>
                                    </div>
                                
                                <!-- </div>
                                <div class="form-group row border-1"> -->
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="pentrega" name="pentrega" placeholder="Persona que devuelve material" required="true">
                                        <label id="dc" style="color:red;"></label>
                                    </div>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="precibe" name="precibe" placeholder="Persona que recibe material" required="true">
                                        <label id="dc" style="color:red;"></label>
                                    </div>

                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-success" id="btndetalle">
                                            <i class="nav-icon fas fa-forward">
                                                <b> Siguiente</b>
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            <!-- </form>    -->
                        </div>
                    </div>

                    <div class="card centrado">
                        <!-- /.card-header -->
                        <div class="card-body" id="detalle">
                            <!-- <form id="form-detalle"> -->
                            <div class="form-group row">

                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-success swalDefaultError" id="btnagregarproducto">
                                        <i class="nav-icon fas fa-plus">
                                            <b></b>
                                        </i>
                                        <b>Agregar detalle</b>
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-sm table-hover" id="t">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Código</th>
                                            <th>Producto</th>
                                            <th>Descripción</th>
                                            <th>Tipo</th>
                                            <th>Cantidad</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-detalle">

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td style="text-align: right;" colspan="5"><b>TOTAL</b></td>
                                            <td id="total"></td>
                                            <td></td>
                                        </tr>
                                    <tfoot>
                                </table>
                            </div>
                           
                            <div class="col-sm-7">
                                <button type="button" class="btn btn-success" id="btnregistrar">
                                    <i class="nav-icon fas fa-cart-plus">
                                        <b>Registrar</b>
                                    </i>
                                </button>
                                <a href="<?php echo RUTA_URL; ?>/devoluciones/" class="btn btn-danger" >
                                    <i class="nav-icon fas fa-window-close">
                                        <b>Cancelar</b>
                                    </i>
                                </a>
                            </div>
                            <!-- </form> -->
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div> 















 <!-- modal para agregar tipo proyecto  -->
 <div class="modal fade" id="addmodal"> 
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Inventario de productos</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body row">
                            <!-- /.card-header -->
                            <div class="card-body col-sm-9">
                                <table id="example1" class="table table-bordered table-striped table-hover table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Producto</th>
                                            <th>Descripción</th>
                                            <!-- <th>Marca </th> -->
                                            <th>Tipo</th>
                                            <!-- <th>Unidad</th> -->
                                            <th>Cantidad</th>
                                            <th>Instalado</th>
                                            <th>Devuelto</th>
                                            <th>Sobrante</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($datos['detallesalida'] as $producto) { 
                                            if ($producto->tipo == 1) {
                                                $tipo = 'Seriado';
                                            }else{
                                                $tipo = 'No Seriado';
                                            } 
                                            $sobrante = ($producto->cantidad - ($producto->instalado + $producto->devuelto));
                                        ?>
                                        <tr inde="<?php echo $producto->idet; ?>" class="inv">
                                            <td><?php echo $producto->codigo; ?> </td>
                                            <td><?php echo $producto->producto; ?></td>
                                            <td><?php echo $producto->descripciono; ?></td>
                                            <!-- <td><?php //echo $producto->marca; ?></td>                                                         -->
                                            <td><?php echo $tipo; ?></td>
                                            <!-- <td><?php //echo $producto->unidad; ?></td> -->
                                            <td><?php echo $producto->cantidad; ?></td>
                                            <td><?php echo $producto->instalado; ?></td>
                                            <td><?php echo $producto->devuelto; ?></td>
                                            <td><?php echo $sobrante; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>                            
                                </table>
                            </div>
                            <!-- /.card-body -->
                            <div class="col-sm-3">
                                 
                                <input type="hidden" class="form-control" id="idpr">
                                Código: 
                                <input type="text" class="form-control" id="codigo">
                                Producto: 
                                <input type="text" class="form-control" id="producto">
                                <label id="pro" style="color:red;"></label>
                                Descripción:
                                <input type="text" class="form-control" id="desc">
                                <!-- Tipo: -->
                                <input type="hidden" class="form-control" id="tipo">
                                Cant. Ruta:
                                <input type="number" class="form-control" id="exist">
                                Cant. Instalado:
                                <input type="number" class="form-control" id="install">
                                Cant. Sobrante:
                                <input type="number" class="form-control" id="sobrante">
                                Cantidad a entregar:
                                <input type="number" class="form-control" id="cantidad">
                                <label id="cant" style="color:red;"></label>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button id="btnadd" class="btn btn-primary">Agregar detalle</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>





















        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/devoluciones.js"></script>

        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                });
            });
        </script>
    </body>
</html>