<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.css">
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Inventario</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">                

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header row">
                                        <div class="col-sm-4">
                                            <h3 class="card-title float-sm-left">Control de Inventario <?php echo $datos['tipo']; ?></h3>  
                                        </div>  

                                        <div class="col-sm-3">
                                            <!-- <a href="<?php //echo RUTA_URL; ?><?php //echo $datos['urls']; ?>" class="float-sm-right btn btn-info">
                                                <i class="nav-icon fas fa-file-alt"> 
                                                    <b> Inventario <?php //echo $datos['tipos']; ?></b>
                                                </i>
                                            </a> -->
                                        </div>

                                        <div class="col-sm-3">
                                            <!-- <a href="<?php //echo RUTA_URL; ?><?php //echo $datos['urlns']; ?>" class="float-sm-right btn btn-info">
                                                <i class="nav-icon fas fa-file-alt"> 
                                                <b> Inventario <?php //echo $datos['tipons']; ?></b>
                                                </i>
                                            </a>   -->
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="<?php echo RUTA_URL; ?><?php echo $datos['urlng']; ?>" target="_blank" title="Imprimir inventario" class="float-sm-right btn btn-secondary">
                                                <i class="nav-icon fas fa-print"> 
                                                <b> Imprimir</b>
                                                </i>
                                            </a>  
                                        </div>                         
                                    </div>
                                    
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="example1" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Código</th>
                                                    <th>Producto</th>
                                                    <th>Descripción</th>
                                                    <th>Marca </th>
                                                    <th>Tipo</th>
                                                    <th>Unidad</th>
                                                    <th>Existencias</th>
                                                    <!-- <th>Acciones</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($datos['inventarios'] as $producto) { ?>
                                                    <tr inde="<?php echo $producto->code; ?>">
                                                        <td><?php echo $producto->codigo; ?> </td>
                                                        <td><?php echo $producto->nombre; ?></td>
                                                        <td><?php echo $producto->descripcion; ?></td>
                                                        <td><?php echo $producto->marca; ?></td>                                                        
                                                        <td>                                                             
                                                            <?php
                                                                if ($producto->tipo == 1) {
                                                                    echo 'Seriado';
                                                                }else{
                                                                    echo 'No Seriado';
                                                                } 
                                                             ?>
                                                        </td>
                                                        <td><?php echo $producto->unidad; ?></td>
                                                        <td><?php echo $producto->cantidad; ?></td>
                                                        <!-- <td> -->
                                                            <!-- <a href="<?php //echo RUTA_URL; ?>/productos/editar/<?php //echo $producto->code; ?>" title="Editar Producto" class="btn btn-primary"><i class='nav-icon fas fa-edit'></i></a> -->
                                                            <!-- <button title="Eliminar Producto" class="btn btn-danger delete"><i class='nav-icon fas fa-trash-alt'></i></button> -->
                                                        <!-- </td> -->
                                                    </tr>
                                                <?php } ?>
                                            </tbody>                            
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>

        <!-- modal para eliminar cliente  -->
        <div class="modal fade" id="elimproducto">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="encab">Formulario de confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta1" value="<?php echo RUTA_URL;?>" readonly>
                        <input type="hidden" id="e">
                        <div class="modal-body justify-center">
                            <h3>¿Desea eliminar este registro?</h3>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button class="btn btn-danger" data-dismiss="modal">CERRAR</button>
                            <button class="btn btn-primary" id="borrarprod">Aceptar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>   

         <!-- modal para confirmar eliminacion  -->
         <div class="modal fade" id="okis">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 id="encab">Transacción realizada correctamente</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <a href="<?php echo RUTA_URL; ?>/productos/" class="btn btn-success form-control" id="ok">OK</a>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <!-- <button class="btn btn-danger" data-dismiss="modal">CERRAR</button> -->
                            <!-- <button class="btn btn-primary" id="ok">Aceptar</button> -->
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/productos.js"></script>
        <script src="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.js"></script>

        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                });
            });
        </script>
    </body>
</html>