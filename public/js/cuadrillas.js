$(document).ready(function(){
    $('#code').attr('disabled',true);
    $('#e').attr('disabled',true);

    //muestra modal para agregar cuadrilla
    $(document).on('click','#addcuad',function(){
        $('#cuadr').val('');
        $('#encarg').val('');
        $('#enc').html('');
        $('#nom').html('');
        $('#addcuadrilla').modal('show');
    });

    //muestra modal para editar cuadrilla
    $(document).on('click','.editcuad',function(){
        let id = parseInt($(this).parents("tr").find("td").eq(0).text());
        let nombre = $(this).parents('tr').find('td').eq(1).text();
        let encargado = $(this).parents('tr').find('td').eq(2).text();
        $('#code').val(id);
        $('#cuadr1').val(nombre);
        $('#encarg1').val(encargado);
        $('#editcuadrilla').modal('show');
    });

    //agrega cuadrilla
    $(document).on('click','#btnaddcuadri',function(){
        if (document.getElementById('cuadr').value =='' && document.getElementById('encarg').value=='') {
            $('#nom').html('* Este campo es obligatorio');
            $('#enc').html('* Este campo es obligatorio');
        }else if (document.getElementById('cuadr').value =='' && document.getElementById('encarg').value!=''){
                $('#nom').html('* Este campo es obligatorio');
                $('#enc').html('');
        }else if (document.getElementById('cuadr').value !='' && document.getElementById('encarg').value==''){
                $('#enc').html('* Este campo es obligatorio');
                $('#nom').html('');
        }else{
            let nombre = $('#cuadr').val();
            let encargado = $('#encarg').val();
            let url = $('#ruta').val();

            let postData = {
                nombre:nombre,
                encargado:encargado
            };

            $.post(url+'/cuadrillas/guardar',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Cuadrilla Registrada Exitosamente');
                        $('#cuadr').val('');
                        $('#encarg').val('');
                        $('#addcuadrilla').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('La cuadrilla que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    //editar cuadrilla
    $(document).on('click','#btneditcuadri',function(){
        if (document.getElementById('cuadr1').value =='' && document.getElementById('encarg1').value=='') {
            $('#nom1').html('* Este campo es obligatorio');
            $('#enc1').html('* Este campo es obligatorio');
        }else if (document.getElementById('cuadr1').value =='' && document.getElementById('encarg1').value!=''){
                $('#nom1').html('* Este campo es obligatorio');
                $('#enc1').html('');
        }else if (document.getElementById('cuadr1').value !='' && document.getElementById('encarg1').value==''){
                $('#enc1').html('* Este campo es obligatorio');
                $('#nom1').html('');
        }else{
            let nombre = $('#cuadr1').val();
            let encargado = $('#encarg1').val();
            let code = $('#code').val();
            let url = $('#ruta').val();

            let postData = {
                nombre:nombre,
                encargado:encargado,
                code:code
            };

            $.post(url+'/cuadrillas/update',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Cuadrilla Registrada Exitosamente');
                        $('#cuadr1').val('');
                        $('#encarg1').val('');
                        $('#editcuadrilla').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('La cuadrilla que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    $(document).on('click','.delete',function(){
        let idcuad = $(this).parents('tr').find('td').eq(0).text();
       $('#e').val(idcuad);
        $('#elimcuad').modal('show');
    });

    $(document).on('click','#borrarcuad',function(){
        let url = $('#ruta1').val();
        let idc = $('#e').val();
        postData = {
            idc:idc
        };
        
        $.post(url+'/cuadrillas/eliminar',postData,function(response){
            if (response == '1') {                
                    toastr.success('Registro eliminado correctamente.');
                    $('#e').val('');
                    $('#elimcuad').modal('hide');
                    $('#okis').modal('show');
            }else{
                    toastr.error('Error al eliminar registro, recargue la pagina e intente de nuevo.');
            }
        });
    });
});