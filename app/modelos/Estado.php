<?php
    class Estado
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        public function getEstados(){
            $consulta = 'SELECT * FROM estado_productos order by descripcion asc';
            $this->db->query($consulta);

            $resultado = $this->db->registros();
            return $resultado;
        }

        //guardar estado de productos
        public function agregar($datos){
            $consulta = 'INSERT INTO estado_productos (descripcion) 
                    values(:descripcion)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':descripcion',$datos['descripcion']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //obtener cantidad estados de productos por descripcion
        public function obtenercantestadodesc($desc){
            $consulta = 'SELECT * FROM estado_productos WHERE descripcion=:descripcion';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$desc);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad estados de productos por descripcion para editar
        public function obtenercantestadodescnotid($descripcion,$id){
            $consulta = 'SELECT * FROM estado_productos 
                        WHERE descripcion=:descripcion AND id_estado NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //editar estado de productos
        public function editar($datos){
            $consulta = 'UPDATE estado_productos set descripcion = :descripcion
                        where id_estado = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar estado de productos
        public function eliminar($datos){
            $consulta = 'DELETE FROM estado_productos WHERE id_estado=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

    }
    