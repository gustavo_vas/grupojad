    <footer class="main-footer">
    <strong>Copyright &copy; 2019 <a href="#"></a>.</strong>
    <!-- <strong>Copyright &copy; 2019 <a href="#">LuxCodeSV</a>.</strong> -->
    Todos los derechos reservados.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
    </div>
    </footer>

    <!-- jQuery -->
    <script src="<?php echo RUTA_URL;?>/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo RUTA_URL;?>/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 muestra en las notoficaciones-->
    <script src="<?php echo RUTA_URL;?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="<?php echo RUTA_URL;?>/plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo RUTA_URL;?>/plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <!-- <script src="<?php //echo RUTA_URL;?>/plugins/jqvmap/jquery.vmap.min.js"></script> -->
    <!-- <script src="<?php //echo RUTA_URL;?>/plugins/jqvmap/maps/jquery.vmap.world.js"></script> -->
    <!-- jQuery Knob Chart -->
    <script src="<?php echo RUTA_URL;?>/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="<?php echo RUTA_URL;?>/plugins/moment/moment.min.js"></script>
    <script src="<?php echo RUTA_URL;?>/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 este trabaja con los dashboar de graficas especificamente-->
    <script src="<?php echo RUTA_URL;?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote este trabaja con los dashboard para mostrar su contenido como graficas-->
    <script src="<?php echo RUTA_URL;?>/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars este oculta y cambia el estilo del scroll en la barra de menu-->
    <script src="<?php echo RUTA_URL;?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo RUTA_URL;?>/plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App este se encarga de ocultar y mostrar las barra de menu y la barra de colores-->

    <!-- <script src="<?php //echo RUTA_URL;?>/dist/js/adminlte.js"></script> -->
    
    <!-- AdminLTE dashboard demo (This is only for demo purposes) este muestra en los dashboard los mapas y graficas -->
    <script src="<?php echo RUTA_URL;?>/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes este muestra en una barra lateral iconos de colores para personalizar el color de las barras de navegacion--> 
    <script src="<?php echo RUTA_URL;?>/dist/js/demo.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo RUTA_URL;?>/dist/js/adminlte.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo RUTA_URL;?>/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo RUTA_URL;?>/plugins/datatables/dataTables.bootstrap4.js"></script>