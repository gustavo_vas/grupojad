<?php
    class Marca
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        public function getMarcas(){
            $consulta = 'SELECT * FROM marca order by nombre asc';
            $this->db->query($consulta);

            $resultado = $this->db->registros();
            return $resultado;
        }

        //obtener cantidad marcas por nombre
        public function obtenercantmarcanombre($marca){
            $consulta = 'SELECT * FROM marca WHERE nombre=:nombre';
            $this->db->query($consulta);

            $this->db->bind(':nombre',$marca);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad marcas por nombre para editar
        public function obtenercantmarcanombrenotid($marca,$id){
            $consulta = 'SELECT * FROM marca 
                        WHERE nombre=:nombre AND id_marca NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':nombre',$marca);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //guardar marca
        public function agregarmarca($datos){
            $consulta = 'INSERT INTO marca (nombre,descripcion) 
                    values(:nombre,:descripcion)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['marca']);
            $this->db->bind(':descripcion',$datos['descripcion']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //editar marca
        public function editarMarca($datos){
            $consulta = 'UPDATE marca set nombre = :nombre,descripcion = :descripcion
                        where id_marca = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['marca']);
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar marca
        public function eliminarMarca($datos){
            $consulta = 'DELETE FROM marca WHERE id_marca=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
        
    }
    