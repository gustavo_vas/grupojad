<?php
    class Productos extends Controlador
    {
        public function __construct(){
            Sesion::start();
            $this->marcaModelo = $this->modelo('Marca');
            $this->productoModelo = $this->modelo('Producto');
            $this->unidadModelo = $this->modelo('Unidad');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $productos = $this->productoModelo->getproductos();
                $datos = [
                    'productos'=>$productos
                ];
                $this->vista('/productos/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //crear producto
        public function crear(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $marcas = $this->marcaModelo->getMarcas();
                $unidades = $this->unidadModelo->getUnidades();

                $datos = [
                    'marcas'=>$marcas,
                    'unidades'=>$unidades
                ];
                $this->vista('/productos/crear',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //guarda un producto
        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $datos = [
                        'codigo' => $_POST['codigo'],
                        'nombre' => $_POST['nombre'],
                        'descripcion' => $_POST['descripcion'],
                        'marca' => $_POST['marca'],
                        'tipo' => $_POST['tipo'],
                        'unidades' => $_POST['unidades']
                    ];

                    if($this->productoModelo->agregarProducto($datos)){
                        redireccionar('/productos');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                    
                }
                $this->vista('/productos/crear');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //edita un producto
        public function editar($id){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if (is_numeric($id)) {
                    $producto = $this->productoModelo->obtenerProductoId($id);
                    $marcas = $this->marcaModelo->getMarcas();
                    $unidades = $this->unidadModelo->getUnidades();
                    $datos = [
                        'id'=>$id,
                        'codigo'=>$producto->codigo,
                        'nombre'=>$producto->nombre,
                        'descripcion'=>$producto->descripcion,
                        'marca'=>$producto->marca,
                        'tipo'=>$producto->tipo,
                        'unidad'=>$producto->unidad,
                        'codemarca'=>$producto->codemarca,
                        'codeunidad'=>$producto->codeunidad,
                        'marcas'=>$marcas,
                        'unidades'=>$unidades
                    ];
                    $this->vista('/productos/editar',$datos);
                }
                redireccionar('/productos/index');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //actualiza un producto
        public function update($id){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (is_numeric($id)) {
                        $datos = [
                            'id'=>$id,
                            'codigo' => $_POST['codigo'],
                            'nombre' => $_POST['nombre'],
                            'descripcion' => $_POST['descripcion'],
                            'marca' => $_POST['marca'],
                            'tipo' => $_POST['tipo'],
                            'unidades' => $_POST['unidades']
                        ];

                        if($this->productoModelo->updateProducto($datos)){
                            redireccionar('/productos/');
                        }else{
                            die('Ocurrio un Problema al Insertar los datos');
                        }

                    }
                    redireccionar('/productos/');
                    
                }
                $this->vista('/productos/index');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //elimina un producto
        public function eliminar(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $datos = [
                        'id'=>$_POST['id']
                    ];

                    if ($this->productoModelo->eliminarProducto($datos)) {
                        echo '1';//se elimino correctamente
                    }else{
                        echo '0';//error al eliminar registro
                    }
                }else{
                    echo '3';//post no enviado
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
    }
    