<?php
    class Tecnico
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //obtener tecnicos
        public function getTecnicos(){
            $consulta = 'SELECT (tc.id_tecnico)as code,(tc.nombre)as nombre, (tc.direccion)as direccion,(tc.dui)as dui,(tc.telefono)as tel,
                        (tc.correo)as correo,(cd.nombre_cuadrilla)as cuadrilla 
                        FROM tecnicos tc 
                        join cuadrillas cd on tc.id_cuadrilla = cd.id_cuadrilla 
                        order by tc.nombre asc';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //obtener tecnicos por id
        public function obtenerTecnicoId($id){
            $consulta = 'SELECT (tc.id_tecnico)as code,(tc.nombre)as nombre, (tc.direccion)as direccion,(tc.dui)as dui,(tc.telefono)as tel,
                        (tc.correo)as correo,(cd.nombre_cuadrilla)as cuadrilla, (tc.id_cuadrilla)as codecuad
                        FROM tecnicos tc 
                        join cuadrillas cd on tc.id_cuadrilla = cd.id_cuadrilla 
                        where tc.id_tecnico = :id
                        order by tc.nombre asc';
            
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        //guardar un usuario
        public function agregarTecnico($datos){
            $consulta = 'INSERT INTO tecnicos (nombre,direccion,dui,telefono,correo,id_cuadrilla) 
                    values(:nombre,:direccion,:dui,:tel,:correo,:codecuadrilla)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':direccion',$datos['direccion']);
            $this->db->bind(':dui',$datos['dui']);
            $this->db->bind(':tel',$datos['telefono']);
            $this->db->bind(':correo',$datos['correo']);
            $this->db->bind(':codecuadrilla',$datos['cuad']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //modificar un tecnico
        public function updateTecnico($datos){
            $consulta = 'UPDATE tecnicos set nombre = :nombre, direccion = :direccion, 
                    dui = :dui, telefono = :tel, correo = :correo, id_cuadrilla = :codecuadrilla 
                    where id_tecnico = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':direccion',$datos['direccion']);
            $this->db->bind(':dui',$datos['dui']);
            $this->db->bind(':tel',$datos['telefono']);
            $this->db->bind(':correo',$datos['correo']);
            $this->db->bind(':codecuadrilla',$datos['cuad']);
            $this->db->bind(':id',$datos['id']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //elimina un tecnico
        public function eliminarTecnico($datos){
            $consulta = 'DELETE FROM tecnicos where id_tecnico=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['id']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
        
    }
    