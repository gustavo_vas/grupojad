<?php
    class Usuarios extends Controlador
    {
        public function __construct(){
            Sesion::start();
            $this->usuarioModelo = $this->modelo('Usuario');
        }

        public function index(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                $usuarios = $this->usuarioModelo->obtenerUsuarios();

                $datos = [
                    'users'=>$usuarios
                ];
                $this->vista('/usuarios/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //crear un usuario
        public function crear(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                $this->vista('/usuarios/crear');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //guarda un usuario
        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $datos = [
                        'nombre' => $_POST['nombres'],
                        'apellido' => $_POST['apellidos'],
                        'correo' => $_POST['correo'],
                        'telefono' => $_POST['telefono'],
                        'tipo' => $_POST['tipo_user'],
                        'user' => $_POST['usuario'],
                        'pass' => $_POST['password'],
                        'estado' => $_POST['estado']
                    ];

                    if($this->usuarioModelo->agregarUsuario($datos)){
                        redireccionar('/usuarios');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                    
                }
                $this->vista('/usuarios/crear');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //edita un usuario
        public function editar($id){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if (is_numeric($id)) {
                    $usuario = $this->usuarioModelo->obtenerUsuarioId($id);
                    $datos = [
                        'id'=>$id,
                        'nombre'=>$usuario->nombre,
                        'ape'=>$usuario->apellido,
                        'email'=>$usuario->correo,
                        'tel'=>$usuario->tel,
                        'tipo'=>$usuario->tipo,
                        'estado'=>$usuario->estado
                    ];
                    $this->vista('/usuarios/editar',$datos);
                }
                redireccionar('/usuarios/index');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //actualiza un usuario
        public function update($id){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (is_numeric($id)) {
                        $datos = [
                            'id'=>$id,
                            'nombre' => $_POST['nombres'],
                            'apellido' => $_POST['apellidos'],
                            'correo' => $_POST['correo'],
                            'telefono' => $_POST['telefono'],
                            'tipo' => $_POST['tipo_user'],
                            'estado' => $_POST['estado']
                        ];

                        if($this->usuarioModelo->updateUsuario($datos)){
                            redireccionar('/usuarios');
                        }else{
                            die('Ocurrio un Problema al Insertar los datos');
                        }

                    }
                    redireccionar('/usuarios/index');
                    
                }
                $this->vista('/usuarios/crear');
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
