$(document).ready(function(){
    $('#prod').attr('disabled',true);
    $('#cantidad').attr('disabled',true);
    $('#mas').attr('disabled',true);
    $('#codigo').attr('disabled',true);
    $('#producto').attr('disabled',true);
    $('#desc').attr('disabled',true);
    $('#tipo').attr('disabled',true);
    $('#idpr').attr('disabled',true);
    $('#exist').attr('disabled',true);
    $('#install').attr('disabled',true);
    $('#sobrante').attr('disabled',true);
    $('#detalle').hide(); 

    var datos2 = new Array();

    //abre la seccion de detalle al ingresar el maestro
    $(document).on('click','#btndetalle',function(){
        if (document.getElementById('doc').value =='' && document.getElementById('fecha').value=='') {
            $('#dc').html('* Campo obligatorio');
            $('#fec').html('* Campo obligatorio');
        }else if (document.getElementById('doc').value =='' && document.getElementById('fecha').value!=''){
            $('#dc').html('* Campo obligatorio');
            $('#fec').html('');
        }else if (document.getElementById('doc').value !='' && document.getElementById('fecha').value==''){
            $('#fec').html('* Campo obligatorio');
            $('#dc').html('');
        }else{
            $('#fec').html('');
            $('#dc').html('');
            $('#doc').attr('disabled',true);
            $('#fecha').attr('disabled',true);
            $('#proyectos').attr('disabled',true);
            $('#pentrega').attr('disabled',true);
            $('#precibe').attr('disabled',true);
            $('#btndetalle').attr('disabled',true);

            $('#prod').attr('disabled',false);
            $('#cantidad').attr('disabled',false);
            $('#mas').attr('disabled',false);

            $('#detalle').show();
        }
    });

    //abrir el modal
    $(document).on('click','#btnagregarproducto',function(){
        $('#codigo').val('');
        $('#producto').val('');
        $('#desc').val('');
        $('#tipo').val('');
        $('#idpr').val('');
        $('#cantidad').val('');
        $('#cant').html('');
        $('#pro').html('');
        $('#exist').val('');
        $('#addmodal').modal('show');
    });

    //agrega desde el modal al detalle en el modal
    $(document).on('click','.inv',function(){
        let codigo = $(this).find('td').eq(0).text();
        let producto = $(this).find('td').eq(1).text();
        let desc = $(this).find('td').eq(2).text();
        let tipo = $(this).find('td').eq(3).text();
        let exist = $(this).find('td').eq(4).text();
        let install = $(this).find('td').eq(5).text();
        let sobra = $(this).find('td').eq(7).text();
        let element = $(this)[0];
        let idpr = $(element).attr('inde');
        $('#codigo').val(codigo);
        $('#producto').val(producto);
        $('#desc').val(desc);
        $('#tipo').val(tipo);
        $('#idpr').val(idpr);
        $('#exist').val(exist);
        $('#install').val(install);
        $('#sobrante').val(sobra);
        if(sobra<=0){
            $('#btnadd').attr('disabled',true);
        }else{
            $('#btnadd').attr('disabled',false);
        }
    });

    $('#cantidad').on('input', function () {    
        var value = $(this).val();        
        if ((value !== '') && (value.indexOf('.') === -1)) {            
            $(this).val(Math.max(Math.min(value, $('#sobrante').val()), 0));
        }
    });

    //agregar al detalle desde el modal
    $(document).on('click','#btnadd',function(){
        if (document.getElementById('producto').value =='' && document.getElementById('cantidad').value=='') {
            $('#pro').html('* Este campo es obligatorio');
            $('#cant').html('* Este campo es obligatorio');
        }else if (document.getElementById('producto').value =='' && document.getElementById('cantidad').value!=''){
                $('#pro').html('* Este campo es obligatorio');
                $('#cant').html('');
        }else if (document.getElementById('producto').value !='' && document.getElementById('cantidad').value==''){
                $('#cant').html('* Este campo es obligatorio');
                $('#pro').html('');
        }else{
            let total = 0;
            let codigo= $('#codigo').val();
            let producto= $('#producto').val();
            let desc= $('#desc').val();
            let tipo= $('#tipo').val();
            let id= $('#idpr').val();
            let cantidad= $('#cantidad').val();

            var datos = [];
            let template = '';

            datos = {
                id:id,
                cant: cantidad,
                codigo: codigo,
                producto: producto,
                desc:desc,
                tipo:tipo,                
            };
            datos2.push(datos);
            $("#table-detalle").empty();

            for(var i = 0; i < datos2.length; i++) {
                total += parseInt(datos2[i].cant);
                template += `<tr inde='${i}'>
                <td>${datos2[i].id}</td>
                <td>${datos2[i].codigo}</td>
                <td>${datos2[i].producto}</td>
                <td>${datos2[i].desc}</td>
                <td>${datos2[i].tipo}</td>
                <td>${datos2[i].cant}</td>
                <td><button type="button" class="btn btn-danger d"><i class="nav-icon fas fa-trash-alt"></i></button></td>'+
                </tr>`;
            }

            $("#table-detalle").prepend(template);
            document.getElementById("total").innerHTML = total;
            $('#addmodal').modal('hide');
        }
    });

    //metodo jquery para eliminar del array y mostrar en tabla
    $(document).on('click','.d', function(){
        if (confirm('¿desea eliminar este registro?')) {
            let template = '';
            let total = 0;
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('inde');
            datos2.splice(id,1);
            $("#table-detalle").empty();

            for(var i = 0; i < datos2.length; i++) {
                total += parseInt(datos2[i].cant);
                template += `<tr inde='${i}'>
                <td>${datos2[i].id}</td>
                <td>${datos2[i].codigo}</td>
                <td>${datos2[i].producto}</td>
                <td>${datos2[i].desc}</td>
                <td>${datos2[i].tipo}</td>
                <td>${datos2[i].cant}</td>
                <td><button type="button" class="btn btn-danger d"><i class="nav-icon fas fa-trash-alt"></i></button></td>'+
                </tr>`;
            }

            $("#table-detalle").prepend(template);
            document.getElementById("total").innerHTML = total;
        }
    });

    //agregar entrada a bd
    $(document).on('click','#btnregistrar',function(){
        let url = $('#ruta').val();
        let detalle = JSON.stringify(datos2);
        let documento = $('#doc').val();
        let fentrega = $('#fecha').val();
        let pentrega = $('#pentrega').val();
        let precibe = $('#precibe').val();

        let postData = {
            documento:documento,
            fentrega:fentrega,
            pentrega:pentrega,
            precibe:precibe,
            detalle:detalle
        };

        $.post(url+'/devoluciones/guardar',postData,function(response){
                $(location).attr('href',url+'/devoluciones/index');            
        });
    });
});