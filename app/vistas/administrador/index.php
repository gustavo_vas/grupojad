<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Principal</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                    <li class="breadcrumb-item active">Principal</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                    <h3><?php echo $datos['cantidad'] ?></h3>

                    <p>Catálogo de materiales</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-bag"></i>
                    </div>
                    <a href="<?php echo RUTA_URL; ?>/productos/" class="small-box-footer">Panel Catálogo de productos <i class="fas fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                    <h3><?php echo $datos['inv'] ?><sup style="font-size: 20px"></sup></h3>

                    <p>Inventario General</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="<?php echo RUTA_URL; ?>/inventarios/" class="small-box-footer">Panel de Inventario <i class="fas fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                    <h3><?php echo $datos['tect'] ?></h3>

                    <p>Técnicos</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-person-add"></i>
                    </div>
                    <a href="<?php echo RUTA_URL; ?>/tecnicos/" class="small-box-footer">Panel de Técnicos <i class="fas fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                    <h3><?php echo $datos['proy'] ?></h3>

                    <p>Proyectos</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?php echo RUTA_URL; ?>/proyectos/" class="small-box-footer">Panel de proyectos <i class="fas fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
            </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                    <h3><?php echo $datos['entrada'] ?></h3>

                    <p>Entrada de material</p>
                    </div>
                    <div class="icon">
                    <i class="nav-icon fas fa-download"></i>
                    </div>
                    <a href="<?php echo RUTA_URL; ?>/entradas/" class="small-box-footer">Panel Entradas de material <i class="fas fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                    <h3><?php echo $datos['salida'] ?><sup style="font-size: 20px"></sup></h3>

                    <p>Salida y entrega de material</p>
                    </div>
                    <div class="icon">
                    <i class="nav-icon fas fa-upload"></i>
                    </div>
                    <a href="<?php echo RUTA_URL; ?>/salidas/" class="small-box-footer">Panel de salidas <i class="fas fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                    <h3><?php echo $datos['salida'] ?></h3>

                    <p>Instalaciones</p>
                    </div>
                    <div class="icon">
                    <i class="nav-icon fas fa-cog"></i>
                    </div>
                    <a href="<?php echo RUTA_URL; ?>/salidas/instalaciones" class="small-box-footer">Panel de instalaciones <i class="fas fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                    <h3><?php echo $datos['dev'] ?></h3>

                    <p>Devoluciones</p>
                    </div>
                    <div class="icon">
                    <i class="nav-icon fas fa-truck"></i>
                    </div>
                    <a href="<?php echo RUTA_URL; ?>/devoluciones/" class="small-box-footer">Panel de devoluciones <i class="fas fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
            </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
    </body>
</html>