<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Registro de Productos</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5">
                                <form action="<?php echo RUTA_URL; ?>/productos/guardar" method="post">
                                    <div class="form-group row">
                                        <label for="codigo" class="col-sm-3">Código</label>
                                        <input type="text" class="form-control col-sm-9" name="codigo" placeholder="Ingresar Código" required="true">    
                                    </div>

                                    <div class="form-group row">
                                        <label for="nombre" class="col-sm-3">Nombre</label>
                                        <input type="text" class="form-control col-sm-9" name="nombre" placeholder="Ingresar nombre" required="true">  
                                    </div>

                                <div class="form-group row">
                                    <label for="descripcion" class="col-sm-3">Descripción</label>
                                    <input type="text" class="form-control col-sm-9" name="descripcion" placeholder="Ingresar descripcion" required="true"> 
                                </div>

                                <div class="form-group row">
                                    <label for="marca" class="col-sm-3">Marca</label>
                                    <select class="form-control col-sm-9" name="marca" required="true">
                                        <?php 
                                            foreach ($datos['marcas'] as $marca) {
                                                echo '<option value="'.$marca->id_marca.'">'.$marca->nombre.'</option>';
                                            }
                                        ?>
                                    </select>    
                                </div>

                                <div class="form-group row">
                                    <label for="tipo" class="col-sm-3">Tipo</label>
                                    <select class="form-control col-sm-9" name="tipo" required="true">
                                        <option value="1">Seriado</option>
                                        <option value="2">No Seriado</option>
                                    </select>    
                                </div>

                                <div class="form-group row">
                                    <label for="unidades" class="col-sm-3">Unidades</label>
                                    <select class="form-control col-sm-9" name="unidades" required="true">
                                        <?php 
                                            foreach ($datos['unidades'] as $unidad) {
                                                echo '<option value="'.$unidad->id_unidad.'">'.$unidad->descripcion.'</option>';
                                            }
                                        ?>
                                    </select>    
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Registrar</button>
                                <a href="<?php echo RUTA_URL; ?>/productos/">
                                    <button type="button" class="btn btn-danger">Cancelar</button>
                                </a>
                                </form>
                            </div>
                        </div>
                    </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
    </body>
</html>