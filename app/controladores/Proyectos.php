<?php
 class Proyectos extends Controlador
 {
     public function __construct(){
         Sesion::start();
         $this->proyectoModelo = $this->modelo('Proyecto');
         $this->tiproyectoModelo = $this->modelo('Tproyecto');
         $this->cuadrillaModelo = $this->modelo('Cuadrilla');
     }

     public function index(){
        if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
            $proyectos = $this->proyectoModelo->getproyectos();
            $datos = [
                'proyectos'=>$proyectos
            ];
            $this->vista('/proyectos/index',$datos);
        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    //crear proyecto
    public function crear(){
        if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
            $tipoproyectos = $this->tiproyectoModelo->getTipos();
            $cuadrillas = $this->cuadrillaModelo->obtenercuadrillas();

            $datos = [
                'tiposp'=>$tipoproyectos,
                'cuadrillas'=>$cuadrillas
            ];
            $this->vista('/proyectos/crear',$datos);
        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    //guarda un proyecto
    public function guardar(){
        if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $datos = [
                    'numero' => $_POST['numero'],
                    'fcreado' => $_POST['fcreado'],
                    'cuadrilla' => $_POST['cuadrilla'],
                    'tiposp' => $_POST['tiposp']
                ];

                if($this->proyectoModelo->agregarProyecto($datos)){
                    redireccionar('/proyectos');
                }else{
                    die('Ocurrio un Problema al Insertar los datos');
                }
                
            }
            $this->vista('/proyectos/crear');
        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    //edita un proyecto
    public function editar($id){
        if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
            if (is_numeric($id)) {
                $proyectos = $this->proyectoModelo->obtenerProyectoId($id);
                $tipoproyectos = $this->tiproyectoModelo->getTipos();
                $cuadrillas = $this->cuadrillaModelo->obtenercuadrillas();
                $datos = [
                    'id'=>$id,
                    'numpro'=>$proyectos->numpro,
                    'codetipop'=>$proyectos->codetipopro,
                    'codecuadrilla'=>$proyectos->codecuadrilla,
                    'tiposps'=>$proyectos->tipopro,
                    'cuadrilla'=>$proyectos->cuadrilla,
                    'tiposp'=>$tipoproyectos,
                    'cuadrillas'=>$cuadrillas
                ];

                $this->vista('/proyectos/editar',$datos);
            }
            redireccionar('/proyectos/index');
        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    //actualiza un proyecto
    public function update($id){
        if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if (is_numeric($id)) {
                    $datos = [
                        'id' => $id,
                        'numero' => $_POST['numero'],
                        'fupdate' => $_POST['fupdate'],
                        'cuadrilla' => $_POST['cuadrilla'],
                        'tiposp' => $_POST['tiposp']
                    ];

                    if($this->proyectoModelo->updateProyecto($datos)){
                        redireccionar('/proyectos/');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }

                }
                redireccionar('/proyectos/');
                
            }
            $this->vista('/productos/index');
        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    //elimina un proyecto
    public function eliminar(){
        if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $datos = [
                    'id'=>$_POST['id']
                ];

                if ($this->proyectoModelo->eliminar($datos)) {
                    echo '1';//se elimino correctamente
                }else{
                    echo '0';//error al eliminar registro
                }
            }else{
                echo '3';//post no enviado
            }
        }else{
            redireccionar('/errores/destroySesion');
        }
    }
     
 }
 