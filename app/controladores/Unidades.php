<?php
    class Unidades extends Controlador
    {
        public function __construct(){
            Sesion::start();            
            $this->unidadModelo = $this->modelo('Unidad');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $unidades = $this->unidadModelo->getUnidades();
                $datos = [
                    'unidades'=>$unidades
                ];
                $this->vista('/unidades/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $descripcions = $this->unidadModelo->obtenercantunidaddesc($descripcion);

                    if ($descripcions==0) {
                        $datos = [
                            'cant' => $_POST['cant'],
                            'descripcion' => $_POST['descripcion']
                        ];
    
                        if($this->unidadModelo->agregarunidad($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function update(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $id = $_POST['code'];
                    $descripcions = $this->unidadModelo->obtenercantunidaddescnotid($descripcion,$id);

                    if ($descripcions==0) {
                        $datos = [
                            'cant' => $_POST['cant'],
                            'descripcion' => $_POST['descripcion'],
                            'code' => $_POST['code']
                        ];
    
                        if($this->unidadModelo->editarunidad($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function eliminar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $datos = [
                            'code' => $_POST['idc']
                        ];
    
                        if($this->unidadModelo->eliminarunidad($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }                  
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    