<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device=width,initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="id=edge">
        <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>/css/estilos.css">
        <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>/dist/css/adminlte.min.css">
        <title><?php echo NOMBRESITIO; ?></title> 
        <style type="text/css">
            .container-login100 {
                background-image: url(<?php echo RUTA_URL;?>/img/fondo.jpg);
                background-position: center center;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
                background-color: #66999;
            }
            .wrap-login100{
                background-color:rgb(0,0,0,0.9);
                border-radius: 15px;
            }
            .login100-form-title, .input100, .focus-input100{
                color: white;
            }
        </style>
    </head>
    <body class="hold-transition login-page container-login100">
        <div class="login-box">
                <div class="card card-info">
                    <div class="card-header">
                        <h2 class="card-title">Grupo JAD</h2>
                    </div>
                </div>
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Inicio de Sesion</h3>
                </div>
                <form class="form-horizontal" action='<?php echo RUTA_URL; ?>/login/login' method='POST'>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Usuario</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Contraseña</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="clave" name="clave" placeholder="Contraseña" required="true">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info btn-lg btn-block">Iniciar Sesion</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/main.js"></script>
    </body>
</html>
