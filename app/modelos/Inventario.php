<?php
    class Inventario
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //obtener productos
        public function getinventarios(){
            $consulta = 'SELECT (inv.id_inventario)as codeinv, (inv.id_producto)as code,(pro.codigo)as codigo,
                        (pro.nombre)as nombre, 
                        (pro.descripcion)as descripcion,(pro.id_marca)as codemarca,(pro.tipo_producto)as tipo,
                        (pro.id_unidad)as codeunidad,(mc.nombre)as marca, (un.descripcion)as unidad,
                        (inv.cantidad_bodega)as cantidad,(inv.cantidad_ruta)as cantidadruta,
                        (inv.cantidad_instalado)as cantidadinstalado,
                        (inv.cant_devuelt)as devuelto
                        FROM inventario inv 
                        join productos pro on inv.id_producto = pro.id_producto 
                        join marca mc on pro.id_marca = mc.id_marca 
                        join unidades un on pro.id_unidad = un.id_unidad 
                        order by inv.cantidad_bodega asc';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //obtener inventario seriado
        public function getinventariosSeriados(){
            $consulta = 'SELECT (inv.id_inventario)as codeinv, (inv.id_producto)as code,(pro.codigo)as codigo,
                        (pro.nombre)as nombre, 
                        (pro.descripcion)as descripcion,(pro.id_marca)as codemarca,(pro.tipo_producto)as tipo,
                        (pro.id_unidad)as codeunidad,(mc.nombre)as marca, (un.descripcion)as unidad,
                        (inv.cantidad_bodega)as cantidad,(inv.cantidad_ruta)as cantidadruta,
                        (inv.cantidad_instalado)as cantidadinstalado,
                        (inv.cant_devuelt)as devuelto
                        FROM inventario inv 
                        join productos pro on inv.id_producto = pro.id_producto 
                        join marca mc on pro.id_marca = mc.id_marca 
                        join unidades un on pro.id_unidad = un.id_unidad 
                        where pro.tipo_producto = 1
                        order by inv.cantidad_bodega asc';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //obtener inventario no seriado
        public function getinventariosNoSeriados(){
            $consulta = 'SELECT (inv.id_inventario)as codeinv, (inv.id_producto)as code,(pro.codigo)as codigo,
                        (pro.nombre)as nombre, 
                        (pro.descripcion)as descripcion,(pro.id_marca)as codemarca,(pro.tipo_producto)as tipo,
                        (pro.id_unidad)as codeunidad,(mc.nombre)as marca, (un.descripcion)as unidad,
                        (inv.cantidad_bodega)as cantidad,(inv.cantidad_ruta)as cantidadruta,
                        (inv.cantidad_instalado)as cantidadinstalado,(inv.cant_devuelt)as devuelto
                        FROM inventario inv 
                        join productos pro on inv.id_producto = pro.id_producto 
                        join marca mc on pro.id_marca = mc.id_marca 
                        join unidades un on pro.id_unidad = un.id_unidad 
                        where pro.tipo_producto = 2
                        order by inv.cantidad_bodega asc';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

    }
    