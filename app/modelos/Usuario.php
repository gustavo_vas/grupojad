<?php

    class Usuario{
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //metodo para obtener usuario para login
        public function obtenerUsuario($usuario,$pass){
            $consulta = 'SELECT (us.id_usuario)as codusuario,(us.usuario)as usuario,(us.nombres)as nombre,
                    (us.apellidos)as apellido,(us.tipo_user)as tipo,(us.telefono)as tel, (us.correo) as correo,
                    (us.estado) as estado FROM usuarios us
                    where usuario=:usuario and password=:pass';
            $this->db->query($consulta);
            $this->db->bind(':usuario',$usuario);
            $this->db->bind(':pass',$pass);
            $resultado = $this->db->registro();
            return $resultado;
        }

        //metodo para obtener usuarios
        public function obtenerUsuarios(){
            $consulta = 'SELECT (us.id_usuario)as codusuario,(us.usuario)as usuario,(us.nombres)as nombre,
                    (us.apellidos)as apellido,(us.tipo_user)as tipo,(us.telefono)as tel, (us.correo) as correo,
                    (us.estado) as estado FROM usuarios us';
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //guardar un usuario
        public function agregarUsuario($datos){
            $consulta = 'INSERT INTO usuarios (usuario,password,nombres,apellidos,tipo_user,telefono,correo,estado) 
                    values(:user,:pass,:nombre,:apellido,:tipo,:tel,:correo,:estado)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':user',$datos['user']);
            $this->db->bind(':pass',$datos['pass']);
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':apellido',$datos['apellido']);
            $this->db->bind(':tipo',$datos['tipo']);
            $this->db->bind(':tel',$datos['telefono']);
            $this->db->bind(':correo',$datos['correo']);
            $this->db->bind(':estado',$datos['estado']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //metodo para obtener usuario para editar
        public function obtenerUsuarioId($id){
            $consulta = 'SELECT (us.id_usuario)as codusuario,(us.usuario)as usuario,(us.nombres)as nombre,
                    (us.apellidos)as apellido,(us.tipo_user)as tipo,(us.telefono)as tel, (us.correo) as correo,
                    (us.estado) as estado FROM usuarios us
                    where us.id_usuario=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        //modificar un usuario
        public function updateUsuario($datos){
            $consulta = 'UPDATE usuarios set nombres = :nombre, apellidos = :apellido, 
                    tipo_user = :tipo, telefono = :tel, correo = :correo, estado = :estado 
                    where id_usuario = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':apellido',$datos['apellido']);
            $this->db->bind(':tipo',$datos['tipo']);
            $this->db->bind(':tel',$datos['telefono']);
            $this->db->bind(':correo',$datos['correo']);
            $this->db->bind(':estado',$datos['estado']);
            $this->db->bind(':id',$datos['id']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

    }