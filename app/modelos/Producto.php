<?php
    class Producto
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //obtener productos
        public function getproductos(){
            $consulta = 'SELECT (pro.id_producto)as code,(pro.codigo)as codigo,(pro.nombre)as nombre, 
                        (pro.descripcion)as descripcion,(pro.id_marca)as codemarca,(pro.tipo_producto)as tipo,
                        (pro.id_unidad)as codeunidad,(mc.nombre)as marca, (un.descripcion)as unidad
                        FROM productos pro 
                        join marca mc on pro.id_marca = mc.id_marca 
                        join unidades un on pro.id_unidad = un.id_unidad 
                        order by pro.nombre asc';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //guardar un producto
        public function agregarProducto($datos){
            $consulta = 'INSERT INTO productos (codigo,nombre,descripcion,id_marca,tipo_producto,id_unidad) 
                    values(:codigo,:nombre,:descripcion,:codemarca,:tipo,:codeunidad)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':codigo',$datos['codigo']);
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':codemarca',$datos['marca']);
            $this->db->bind(':tipo',$datos['tipo']);
            $this->db->bind(':codeunidad',$datos['unidades']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //obtener productos por id
        public function obtenerProductoId($id){
            $consulta = 'SELECT (pro.id_producto)as code,(pro.codigo)as codigo,(pro.nombre)as nombre, 
                        (pro.descripcion)as descripcion,(pro.id_marca)as codemarca,(pro.tipo_producto)as tipo,
                        (pro.id_unidad)as codeunidad,(mc.nombre)as marca, (un.descripcion)as unidad
                        FROM productos pro 
                        join marca mc on pro.id_marca = mc.id_marca 
                        join unidades un on pro.id_unidad = un.id_unidad 
                        where pro.id_producto = :id
                        order by pro.nombre asc';
            
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        //modificar un tecnico
        public function updateProducto($datos){
            $consulta = 'UPDATE productos set codigo = :codigo, nombre = :nombre, 
                    descripcion = :descripcion, id_marca = :codemarca, tipo_producto = :tipo, id_unidad = :codeunidad 
                    where id_producto = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':codigo',$datos['codigo']);
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':codemarca',$datos['marca']);
            $this->db->bind(':tipo',$datos['tipo']);
            $this->db->bind(':codeunidad',$datos['unidades']);
            $this->db->bind(':id',$datos['id']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //elimina un producto
        public function eliminarProducto($datos){
            $consulta = 'DELETE FROM productos where id_producto=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['id']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //obtener cantidad cuadrillas por nombre para editar
        public function getcantproductos(){
            $consulta = 'SELECT * FROM productos';
            $this->db->query($consulta);

            $resultado = $this->db->rowCount();
            return $resultado;
        }
        
    }
    