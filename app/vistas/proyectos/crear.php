<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Registro de Proyectos</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5">
                                <form action="<?php echo RUTA_URL; ?>/proyectos/guardar" method="post">
                                    <div class="form-group row">
                                        <label for="numero" class="col-sm-3">Número</label>
                                        <input type="text" class="form-control col-sm-9" name="numero" placeholder="Ingresar Número de proyecto" required="true">    
                                    </div>

                                    <div class="form-group row">
                                        <label for="fcreado" class="col-sm-3">Fecha Creado</label>
                                        <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control col-sm-9" name="fcreado" placeholder="Ingresar Fecha de creacion" required="true">  
                                    </div>

                                    <div class="form-group row">
                                        <label for="cuadrilla" class="col-sm-3">Cuadrilla</label>
                                        <select class="form-control col-sm-9" name="cuadrilla" required="true">
                                            <?php 
                                                foreach ($datos['cuadrillas'] as $cuadrilla) {
                                                    echo '<option value="'.$cuadrilla->id_cuadrilla.'">'.$cuadrilla->nombre_cuadrilla.'</option>';
                                                }
                                            ?>
                                        </select>    
                                    </div>

                                    <div class="form-group row">
                                        <label for="tiposp" class="col-sm-3">Tipo Proyecto</label>
                                        <select class="form-control col-sm-9" name="tiposp" required="true">
                                            <?php 
                                                foreach ($datos['tiposp'] as $tipo) {
                                                    echo '<option value="'.$tipo->id_tipo_proyecto.'">'.$tipo->descripcion.'</option>';
                                                }
                                            ?>
                                        </select>    
                                    </div>
                                
                                <button type="submit" class="btn btn-primary">Registrar</button>
                                <a href="<?php echo RUTA_URL; ?>/proyectos/">
                                    <button type="button" class="btn btn-danger">Cancelar</button>
                                </a>
                                </form>
                            </div>
                        </div>
                    </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
    </body>
</html>