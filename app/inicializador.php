<?php
    //cargamos las liberias del directorio librerias de la carpeta app
    require_once 'config/configurar.php';
    require_once 'helpers/url_helper.php';
    require_once 'Reportes/vendor/autoload.php';
    require_once 'fpdf/fpdf.php';
    // require_once 'librerias/Base.php';
    // require_once 'librerias/Controlador.php';
    // require_once 'librerias/Core.php';

    spl_autoload_register(function($nombreClase){
        require_once 'librerias/'.$nombreClase.'.php';
    });