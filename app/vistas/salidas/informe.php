<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Reportes</title>
    <!-- <?php //require_once RUTA_APP."/vistas/include/header.php"; ?> -->
    <link rel="stylesheet" href="<?php echo RUTA_URL;?>/css/style.css" media="all" />
  </head>
  <body>    
    <header class="clearfix">
      <hr>
      <div id="logo">
        <img src="<?php echo RUTA_URL;?>/img/logo1.jpg">
      </div>
      <div id="company">
        <h2 class="name">GRUPO JAD EL SALVADOR S.A. DE C.V.</h2>
        <div>Final 25 Av. Sur, Barrio San Rafael, Santa Ana, Santa Ana.</div>
        <div>(503) 2441-0393</div>
        <div><a href="mailto:grupojad@grupojad.net">grupojad@grupojad.net</a></div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">Creado por:</div>
            <h2 class="name"><?php echo Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser'); ?></h2>
            <!-- <div class="address">796 Silver Harbour, TX 79273, US</div> -->
            <div class="address">
                <?php
                    if(Sesion::getSesion('tipo')==1){
                    echo 'Administrador';
                    }else{
                    echo 'Normal';
                    }
                ?>
            </div>
          <div class="email"><a href="mailto:"><?php echo Sesion::getSesion('correo'); ?></a></div>
        </div>
        <div id="invoice">
          <h1>Entrega de material</h1>
          <div class="date">Fecha de creación: <?php echo date('d/m/Y'); ?></div>
          <!-- <div class="date">Due Date: 30/06/2014</div> -->
        </div>
      </div>

      <div id="details" class="clearfix">
        <div id="client" style="border-left: 6px solid #FFFFFF;">            
            <h1 style="color: #000000; font-size: 1.5em; line-height: 1em; font-weight: normal; margin: 0  0 10px 0;">
                No. Documento: <?php echo $datos['documento']; ?>
            </h1>
            <div class="date">Cuadrilla: <?php echo $datos['nomcuads']; ?></div>
        </div>
        <div id="invoice">
            <h1 style="color: #000000; font-size: 1.5em; line-height: 1em; font-weight: normal; margin: 0  0 10px 0;">
                No. Proyecto: <?php echo $datos['nproy']; ?>
            </h1>
          <div class="date">Fecha de entrega: <?php echo $datos['fentrega']; ?></div>
          <!-- <div class="date">Due Date: 30/06/2014</div> -->
        </div>
      </div>
      
      <div class="card-body">
        <table id="example" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th><b>CODIGO</b></th>
                    <th><b>PRODUCTO</b></th>
                    <th><b>DESCRIPCION</b></th>
                    <!-- <th>Marca </th> -->
                    <th><b>TIPO PROD.</b></th>
                    <!-- <th><b>UNIDAD</b></th> -->
                    <th><b>CANTIDAD</b></th>
                    <!-- <th><b>EN RUTA</b></th> -->
                    <!-- <th><b>INSTALADO</b></th> -->
                    <!-- <th>Acciones</th> -->
                </tr>
            </thead>
            <tbody>
                <?php $total=0; foreach($datos['detalle'] as $detalle){
                    $total += $detalle->cantidad;
                    if ($detalle->tipo == 1) {
                        $tipo = 'Seriado';
                    }else{
                        $tipo = 'No Seriado';
                    }
                    $sobrante=($detalle->cantidad - $detalle->instalado);

                    echo '<tr>';
                        echo '<td style="border: 1px solid black;">'.$detalle->codigo.'</td>';
                        echo '<td style="border: 1px solid black;">'.$detalle->producto.'</td>';
                        echo '<td style="border: 1px solid black;">'.$detalle->descripciono.'</td>';
                        echo '<td style="border: 1px solid black;">'.$tipo.'</td>';
                        echo '<td style="border: 1px solid black;">'.$detalle->cantidad.'</td>';
                        // echo '<td style="border: 1px solid black;">'.$detalle->instalado.'</td>';
                        // echo '<td style="border: 1px solid black;">'.$sobrante.'</td>';
                    echo '</tr>';
                }?>
            </tbody>                            
        </table>
    </div>
    <!-- /.card-body -->

      <div id="thanks"></div>
      <div id="notices" style="border-left: 6px solid #FFFFFF; padding-left: 6px;">

            <div id="details" class="clearfix">
                <div id="client" style="border-left: 6px solid #FFFFFF;">
                    <div><h3>Recibido por: <?php echo $datos['precibe']; ?></h3></div><br>
                    <div><h3>F. _____________________</h3></div>
                </div>
                <div id="invoice">
                    <div><h3>Entregado por: <?php echo $datos['pentrega']; ?></h3></div><br>
                    <div><h3>F. _____________________</h3></div>
                </div>
            </div>

        <div class="notice"><b>GRUPO JAD EL SALVADOR S.A. DE C.V.</b></div>
      </div><br>
    </main>
    <!-- <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer> -->
  </body>
</html>