$(document).ready(function(){
    $('#code').attr('disabled',true);
    $('#e').attr('disabled',true);

    //muestra el modal de agregar tipo de proyecto
    $(document).on('click','#addtipo',function(){
        $('#descripcion').val('');
        $('#desc').html('');
        $('#addtp').modal('show');
    });

    //muestra modal para editar tipo de proyecto
    $(document).on('click','.edittpshow',function(){
        let id = parseInt($(this).parents("tr").find("td").eq(0).text());
        let descripcion = $(this).parents('tr').find('td').eq(1).text();
        $('#code').val(id);
        $('#descripcion1').val(descripcion);
        $('#edittp').modal('show');
    });

    //agrega tipo proyecto
    $(document).on('click','#btnaddtp',function(){
        if (document.getElementById('descripcion').value=='') {
            $('#desc').html('* Este campo es obligatorio');
        }else{
            let descripcion = $('#descripcion').val();
            let url = $('#ruta').val();

            let postData = {
                descripcion:descripcion
            };

            $.post(url+'/tproyectos/guardar',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Tipo de proyecto Registrado Exitosamente');
                        $('#descripcion').val('');
                        $('#addtp').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('El tipo de proyecto que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    //editar tipo de proyecto
    $(document).on('click','#btnedittp',function(){
        if (document.getElementById('descripcion1').value=='') {
            $('#desc1').html('* Este campo es obligatorio');
        }else{
            let descripcion = $('#descripcion1').val();
            let code = $('#code').val();
            let url = $('#ruta').val();

            let postData = {
                descripcion:descripcion,
                code:code
            };

            $.post(url+'/tproyectos/update',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Tipo de proyecto Registrado Exitosamente');
                        $('#descripcion1').val('');
                        $('#edittp').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('El Tipo de proyecto que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    $(document).on('click','.delete',function(){
        let idcuad = $(this).parents('tr').find('td').eq(0).text();
       $('#e').val(idcuad);
        $('#elimtp').modal('show');
    });

    $(document).on('click','#borrartp',function(){
        let url = $('#ruta').val();
        let idc = $('#e').val();
        postData = {
            idc:idc
        };
        
        $.post(url+'/tproyectos/eliminar',postData,function(response){
            if (response == '1') {                
                    toastr.success('Registro eliminado correctamente.');
                    $('#e').val('');
                    $('#elimtp').modal('hide');
                    $('#okis').modal('show');
            }else{
                    toastr.error('Error al eliminar registro, recargue la pagina e intente de nuevo.');
            }
        });
    });
});