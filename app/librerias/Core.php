<?php
    /* 
    Mapear la url ingresada desde el navegador, la cual contiene
    1- controlador
    2- metodo
    3- parametro
    */
    class Core{
        protected $controladorActual = 'Login';
        protected $metodoActual = 'index';
        protected $parametros = [];

        //constructor para cargar la url
        public function __construct(){
            $url = $this->getUrl();

            //buscar en controlador si el controlador existe
            if (file_exists('../app/controladores/'.ucwords($url[0]).'.php')) {
                //si existe se setea o configura como controlador por defecto
                $this->controladorActual = ucwords($url[0]);

                //desmontar el indice
                unset($url[0]);
            }

            //requerir el controlador
            require_once '../app/controladores/'.$this->controladorActual.'.php';
            $this->controladorActual = new $this->controladorActual;

            //verificar la sugunda parte de la url la cual es el metodo
            if (isset($url[1])) {
                if (method_exists($this->controladorActual,$url[1])) {
                    //verificar el metodo
                    $this->metodoActual = $url[1];

                    //desmontar el indice
                    unset($url[1]);
                }
            }
                //obtener parametros
                $this->parametros = $url ? array_values($url) : [];

                //llamar callback con parametros del array
                call_user_func_array([$this->controladorActual,$this->metodoActual],$this->parametros);

        }

        //metodo para obtener la url ingresada en el navegador
        public function getUrl(){
            //condicion para verificar si se ha seteado la url
            if (isset($_GET['url'])) {
                $url = rtrim($_GET['url'], '/'); //limpia de los espacios la url
                $url = filter_var($url, FILTER_SANITIZE_URL); //interpreta la url como tal
                $url = explode('/',$url);//delimita la url

                return $url;
            }
        }
    }