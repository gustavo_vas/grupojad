<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.css">
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Unidades de productos</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/administradores/">Inicio</a></li>
                        <li class="breadcrumb-item active">Principal</li>
                    </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">                

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-sm-6">
                                            <h3 class="card-title float-sm-left">Datos de unidades</h3>  
                                        </div>
                                        <div class="col-sm-12">
                                            <button class="float-sm-right btn btn-success" id="addunidad" title="Agregar Marca">
                                                <i class="nav-icon fas fa-book"> 
                                                    <b>Agregar Unidad</b>
                                                </i>
                                            </button>
                                        </div><!-- /.col -->                         
                                    </div>
                                    
                                    <!-- /.card-header -->
                                    <div class="card-body table-responsive">
                                        <table id="example2" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Descripción</th>
                                                    <th>Cantidad</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($datos['unidades'] as $unidad) { ?>
                                                    <tr>
                                                        <td><?php echo $unidad->id_unidad; ?> </td>
                                                        <td><?php echo $unidad->descripcion; ?></td>
                                                        <td><?php echo $unidad->cant_granel; ?></td>
                                                        <td>
                                                            <button title="Editar Unidad" class="btn btn-primary editunidadshow"><i class='nav-icon fas fa-edit'></i></button>
                                                            <button title="Eliminar Unidad" class="btn btn-danger delete"><i class='nav-icon fas fa-trash-alt'></i></button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>                            
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>

                    </div><!-- /.container-fluid -->
                </section><!-- /.content -->

            <!-- </div> -->
        </div>
        <!-- ./wrapper -->
        <!-- </div> -->

        <!-- modal para agregar unidad  -->
        <div class="modal fade" id="addunidadmodal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Unidades</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="descripcion" class="control-label" id="lbldescripcion">Descripción</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="descripcion" required="true" placeholder="Ingresar Descripcion">
                                    <label id="desc" style="color:red;"></label>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                    <label for="cant" class="control-label" id="lblcant">Cantidad</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="cant" required="true" placeholder="Ingresar cantidad Granel">
                                    <label id="canti" style="color:red;"></label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button id="btnaddunidad" class="btn btn-primary">Guardar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <!-- modal para editar marca  -->
        <div class="modal fade" id="editunidadmodal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Unidades</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                                                    
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="code" class="control-label" id="lblcode">Codigo</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="code" required="true">
                                    <label id="code1" style="color:red;"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="descripcion1" class="control-label" id="lbldescripcion">Descripción</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="descripcion1" required="true" placeholder="Ingresar encargado">
                                    <label id="desc1" style="color:red;"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="cant1" class="control-label" id="lblcant1">Cantidad</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="cant1" required="true" placeholder="Ingresar Cantidad Granel">
                                    <label id="canti1" style="color:red;"></label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button id="btneditunidad" class="btn btn-primary">Modificar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <!-- modal para confirmar eliminacion  -->
        <div class="modal fade" id="okis">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 id="encab">Transacción realizada correctamente</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                            <a href="<?php echo RUTA_URL; ?>/unidades/" class="btn btn-success form-control" id="ok">OK</a>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <!-- modal para eliminar cliente  -->
        <div class="modal fade" id="elimunidad">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="encab">Formulario de confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="e">
                        <div class="modal-body justify-center">
                            <h3>¿Desea eliminar este registro?</h3>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button class="btn btn-danger" data-dismiss="modal">CERRAR</button>
                            <button class="btn btn-primary" id="borrarunidad">Aceptar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>   

        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/unidades.js"></script>
        <script src="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.js"></script>

        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                });
            });
        </script>
    </body>
</html>