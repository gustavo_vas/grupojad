<?php
    class Cuadrillas extends Controlador
    {
        public function __construct(){
            Sesion::start();
            $this->cuadrillaModelo = $this->modelo('Cuadrilla');
        }

        public function index(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                $cuadrillas = $this->cuadrillaModelo->obtenercuadrillas();

                $datos = [
                    'cuadrillas'=>$cuadrillas
                ];
                $this->vista('/cuadrillas/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $nombre = $_POST['nombre'];
                    $cuadrillas = $this->cuadrillaModelo->obtenercantcuadrillanombre($nombre);

                    if ($cuadrillas==0) {
                        $datos = [
                            'nombre' => $_POST['nombre'],
                            'encargado' => $_POST['encargado']
                        ];
    
                        if($this->cuadrillaModelo->agregarCuadrilla($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function update(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $nombre = $_POST['nombre'];
                    $id = $_POST['code'];
                    $cuadrillas = $this->cuadrillaModelo->obtenercantcuadrillanombrenotid($nombre,$id);

                    if ($cuadrillas==0) {
                        $datos = [
                            'nombre' => $_POST['nombre'],
                            'encargado' => $_POST['encargado'],
                            'code' => $_POST['code']
                        ];
    
                        if($this->cuadrillaModelo->editarCuadrilla($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }


        public function eliminar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // $nombre = $_POST['nombre'];
                    // $id = $_POST['code'];
                    // $cuadrillas = $this->cuadrillaModelo->obtenercantcuadrillanombrenotid($nombre,$id);

                    // if ($cuadrillas==0) {
                        $datos = [
                            'code' => $_POST['idc']
                        ];
    
                        if($this->cuadrillaModelo->eliminarCuadrilla($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    // }else{
                    //     echo '2';//ya existe una cuadrilla con ese nombre
                    // }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    