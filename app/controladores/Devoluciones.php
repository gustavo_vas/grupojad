<?php
 class Devoluciones extends Controlador
 {
    public function __construct(){
        Sesion::start();
        $this->devolucionModelo = $this->modelo('Devolucion');
        $this->inventarioModelo = $this->modelo('Inventario');
    }

    public function index(){
        if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
            $devoluciones = $this->devolucionModelo->getDevoluciones();
            $datos = [
                'devoluciones' => $devoluciones
            ];
            $this->vista('/devoluciones/index',$datos);
        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    public function crear(){
        if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
            // $inventarios = $this->inventarioModelo->getinventarios();
            $detallesalida = $this->devolucionModelo->getDetallesalidas();
            $datos = [
                'detallesalida'=>$detallesalida
            ];
            $this->vista('/devoluciones/crear',$datos);
        }else{
            redireccionar('/errores/destroySesion');
        }
    } 

    public function guardar(){
        if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $documento = $_POST['documento'];
                $fentrega = $_POST['fentrega'];
                $pentrega = $_POST['pentrega'];
                $precibe = $_POST['precibe'];
                $fcreado = date('Y-m-d');
                $usuario = Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser');
                $detalles = json_decode($_POST['detalle']);

                $datos = [
                    'documento'=>$documento,
                    'fentrega'=>$fentrega,
                    'pentrega'=>$pentrega,
                    'precibe'=>$precibe,
                    'fcreado'=>$fcreado,
                    'usuario'=>$usuario
                ];

                $iddevolucion = $this->devolucionModelo->agregarDevolucion($datos);
                if($iddevolucion){
                    foreach ($detalles as $det) {
                        $detalle = [
                            'idinv' => $det->id,
                            'cantidad' => $det->cant,
                            'iddevolucion' => $iddevolucion
                        ];
                        // echo $identrada;
                        if($this->devolucionModelo->devolverinventario($detalle)){
                            echo '1';//ingresado
                        }else{
                            echo '2';//error
                        }
                    }
                }else{
                    echo 'no';
                }
                // echo json_encode($detalles);
            }else{
                redireccionar('/entradas/crear');
            }
            
        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    public function show($id){
        if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
            if (is_numeric($id)) {
                $devolucion = $this->devolucionModelo->getdevolucion($id);
                if ($devolucion->documento != '') {
                    $detalle = $this->devolucionModelo->getDetalle($id);
                    $datos = [
                        'documento'=>$devolucion->documento,
                        'fentrega'=>$devolucion->fdev,
                        'pentrega'=>$devolucion->pentrega,
                        'precibe'=>$devolucion->precibe,
                        'detalle'=>$detalle
                    ];
                    $this->vista('/devoluciones/show',$datos);
                }else{
                    redireccionar('/errores/notfound');
                }  
                
            }else{
                redireccionar('/errores/notfound');
            }                
        }else{
            redireccionar('/errore/destroySesion');
        }
    }

    public function reportes($id){
        if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
            if (is_numeric($id)) {
                $devolucion = $this->devolucionModelo->getdevolucion($id);
                if ($devolucion->documento != '') {
                    $detalle = $this->devolucionModelo->getDetalle($id);
                    $datos = [
                        'documento'=>$devolucion->documento,
                        'fentrega'=>$devolucion->fdev,
                        'pentrega'=>$devolucion->pentrega,
                        'precibe'=>$devolucion->precibe,
                        'detalle'=>$detalle
                    ];
                    $this->vista('/devoluciones/informe',$datos);
                }else{
                    redireccionar('/errores/notfound');
                }  
                
            }else{
                redireccionar('/errores/notfound');
            }                
        }else{
            redireccionar('/errore/destroySesion');
        }
    }
 }
 